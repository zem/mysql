# datatype of FIELD_MEASUREMENT_TIME is set to varchar(10) instead of varchar(5)
# so there is no warning of truncated data when loading data from 
# Chromium_Modelling_Query_2NoHeader.csv 
CREATE TABLE `FieldMeasurement` (
  `LOCATION_ID` varchar(30) NOT NULL, 
  `FIELD_PARAMETER` varchar(30) NOT NULL,
  `FIELD_MEASUREMENT_VALUE` varchar(10) NOT NULL,
  `FIELD_MEASUREMENT_UNITS` varchar(10) NOT NULL,
  `FIELD_MEASUREMENT_COMMENTS` varchar(2000) DEFAULT NULL,
  `FIELD_MEASUREMENT_RECNO` int(11) NOT NULL,
  `FIELD_MEASUREMENT_DATE` datetime NOT NULL,
  `FIELD_MEASUREMENT_TIME` varchar(10) NOT NULL,
	PRIMARY KEY (FIELD_MEASUREMENT_RECNO)
) DEFAULT CHARSET=latin1;
