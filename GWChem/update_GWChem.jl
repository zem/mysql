# updates AnalyticalData and FieldMeasurement table with mothly data files
#
# Usage:
# julia update_GWChen.jl data_file table_name
#
# data_file: the monthly data file from ftp site(ftp://ftp.locustec.com/)
# This script takes off the header(first line) of the monthly file first, then load the data
#
# NOTE:
# When there are updates(entries with the same value for the unique field) in the
# input file for the entries that are already in the database,
# then this script updates the table with the new values from the input file.
#
# In one input file, if there are entries with the same value for the 'unique' field,
# LOAD DATA LOCAL INFILE use the first entry and ignores the rest. It is used here
# this way since we do not expect duplicates or updates in the same input file.

import MySQL
import DataFrames


function updateData(host, username, password, database, file, table)
    #have to use 'MYSQL_OPT_LOCAL_INFILE=>1' while connecting so the following LOAD DATA LOCAL INFILE will work
    conn = MySQL.mysql_connect(host, username, password, database, opts = Dict(MySQL.MYSQL_OPT_LOCAL_INFILE=>1))
    MySQL.mysql_execute(conn, "create table temp_table like $table;")
    command = string("LOAD DATA LOCAL INFILE '$file' INTO TABLE temp_table FIELDS TERMINATED BY '\t' OPTIONALLY ENCLOSED BY '\"';")
#    println(command)
    MySQL.mysql_execute(conn, command)
    command = string("INSERT INTO $table SELECT * FROM temp_table ON DUPLICATE KEY UPDATE")	      
    columns = MySQL.mysql_execute(conn, "SHOW COLUMNS FROM $table;")
    for c = 1:size(columns,1)
        column = columns[1][c]
        if c == size(columns,1) #last column
            command = command * " $column = VALUES($column);"
        else
            command = command * " $column = VALUES($column),"
        end
    end
#    println(command)
    MySQL.mysql_execute(conn, command)
    MySQL.mysql_execute(conn, "DROP TABLE temp_table;")
    MySQL.mysql_disconnect(conn)   
end


if (length(ARGS) != 2)
    println("Usage:\njulia update_GWChem.jl data_file table_name")
    exit(0)
end
file = ARGS[1]
table = ARGS[2]
file_noheader = "tmp_monthly_data.txt"
#table = "GroundWaterChemData"
database = "LocalWork"


# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))
print("Enter host name: ")
host = chomp(readline(STDIN))

run(`dos2unix $file`)

# take off the header of the input file
run(pipeline(`sed 1d $file`, file_noheader))

# make backup file
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))

updateData(host, username, password, database, file_noheader, table)

run(`rm $file_noheader`)
