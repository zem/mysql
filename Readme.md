# MYSQL scripts for ZEM

"MYSQL" is a [ZEM](https://gitlab.com/zem) module.

All modules under ZEM are open-source released under GNU GENERAL PUBLIC LICENSE Version 3.

## SitePumping
### SitePumping/SitePumping.jl
Update LocalWork (table PumpTest) with the pumping records associated with field work conducted at the LANL site(MS Excel files). The format of the source data files aren't consistent, so this script normally need to be ajusted to work with the data file.

## access_loads
Files used to process the groundwater level data collected at the intermediate and regional monitoring wells at the LANL site. The data are provided in the form of MS Access database (complied by Shannon Allen).

### access_loads/access_loadsREADME.txt
Detail and notes of how to use data from the MS Access database to update LocalWork(table: GroundWaterLevelData, ScreenData, PortScrn)

### access_loads/clean_db.bat
Deal with the naming difference between the MS Access database and LocalWork. It is run every time LocalWork is updated with the MS Access database.

### access_loads/load_access_file.jl
Update LocalWork (table: GroundWaterLevelData, ScreenData, PortScrn) with the MS Access database.

## countyPumping
Files used to process Los Alamos county water-supply pumping records (complied by Wayne Witten).

### countyPumping/clean_dbProdDaily.bat
Deal with the naming difference between the data from county and LocalWork

### countyPumping/countypumping.jl
Update LocalWork (table ProdDaily) with Los Alamos county water-supply pumping records (MS Excel files).

## database_note

### database_note/setweb/webInterfaceSetup.txt
Document of the process setting up web interface on mads01, including setting up Apache server and CGI/perl

###  database_note/setupLocalWork_newServer.txt
Document of the process setting up database on mads01

## query_tools

### query_tools/CrConPlot.jl
Query Chromium concentration, calculate the average value for each year, and plot the queried data and the average

### query_tools/YearAvgWL.jl
Query and calculate the average water level(piezometricWLft in GroundWaterLevelData table) of all the wells whose screen type(HYDRO_ZONE_TYPE_CODE) are "RT", "R", or "I" for every year

### query_tools/YearMonthAvgWLrt.jl
Calculate the average water level(piezometricWLft in GgoundWaterLevelData table) of all the wells whose scree type(HYDRO_ZONE_TYPE_CODE) is "RT" of a certain month (avgm) in a certain year (avgy). Then check wells in the c_wells list and print out the ones which are not in the query result list.

#### query_tools/latest_record.jl
Query the date of the latest record for each well (or each well in the chromium wells list) from the input table

## reference
### reference/compare_intellus
Compare the geochem data in LocalWork with intellus

### reference/example_plotConcentration
### reference/example_plotConcentrationWaterLevels
### reference/example_plotWaterLevel
Plotting examples.

## weather_machine (Barometric pressure data)
Files used to process Barometric pressure data which are collected at the LANL TA-54 weather station. The data are provided as monthly data feeds by email (WXMach@lanl.gov). The data is also available at http://weather.lanl.gov

### weather_machineREADME
Detail and notes of how to use Barometric pressure data to update LocalWork(table TA54BaroData)

### weather_machine/run_load.py
Update LocalWork (table TA54BaroData) with Barometric pressure data.

weather_machine/run_load.py calls **weather_machine/fixifybaro.py** and **weather_machine/load_raw_ta54_data.sh**



