# julia load_OpsFlowSummary.jl /scratch/sft/DataStreamLocalWork/SitePumping/20180125OpsFlowSummary_1-25-18_stepData.xlsx

import ExcelReaders
import DataFrames
import DataStructures
import MySQL

file = ARGS[1]
table = "PumpTest"
database = "LocalWork"
host = "mads01"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))

# make backup file for $table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))

data = ExcelReaders.readxl(DataFrames.DataFrame, file, "stepFlowData!A2:F77", header=false)

conn = MySQL.mysql_connect(host, username, password, database)

for i = 1:size(data,1) #size(data,1) returns the number of rows in $data
    wellname = data[1][i]
    starttime = Dates.format(data[2][i],"yyyy-mm-dd HH:MM:SS")
    endtime = Dates.format(data[3][i],"yyyy-mm-dd HH:MM:SS")
    pumprate = data[6][i]
    commandstring = string("insert into $table (wellname, pumprategpm, starttime, endtime) values (\"$wellname\", $pumprate, \"$starttime\", \"$endtime\")");
    MySQL.mysql_execute(conn, commandstring)
end





