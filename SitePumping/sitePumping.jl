# Update LocalWork (table PumpTest) with the pumping records(Excel file) associated with field work
# conducted at the LANL site
# The format of the source data files aren't consistent, so this script normally need to be ajusted
# to work with the data file.

import ExcelReaders
import DataFrames
import MySQL

fileName = "20161216PumpTimesFinal.xlsx";
sheets = ["Sheet1","Sheet2","Sheet3"]
lastRowInSheet = [31,36,46]
firstRow = 23

# read the data into a DataFrame from excel input file
function getdata(sheetNum, startRow=firstRow, endRow=lastRowInSheet[sheetNum])
    data = ExcelReaders.readxl(DataFrames.DataFrame, fileName, "$(sheets[sheetNum])!D$startRow:H$endRow", header=false)
    return data
end

function insertdata(data, s, host, username, password, database)
    conn = MySQL.mysql_connect(host, username, password, database)
    #conn = MySQL.mysql_connect("madsmax", "ntao", "ntao", "LocalWork")
    for i = 1:size(data,1) #size(data,1) returns the number of rows in $data
        wellname = data[4][i]
        if wellname == "CrPZ-2"
            starttime = Dates.format(data[1][i],"yyyy-mm-dd HH:MM:SS")
            endtime = Dates.format(data[2][i],"yyyy-mm-dd HH:MM:SS")
            pumprategpm = data[3][i]
            commandstring = string("insert into PumpTest (wellname, pumprategpm, starttime, endtime)
            values (\"$wellname\", $pumprategpm, \"$starttime\", \"$endtime\")");
#            println("$commandstring")
            MySQL.mysql_execute(conn, commandstring)
        else
            starttime = Dates.format(data[1][i],"yyyy-mm-dd HH:MM:SS")
            endtime = Dates.format(data[2][i],"yyyy-mm-dd HH:MM:SS")
            pumprategpm = data[3][i]
            ULI = data[5][i]
            commandstring = string("insert into PumpTest (wellname, pumprategpm, starttime, endtime, ULI)
            values (\"$wellname\", $pumprategpm, \"$starttime\", \"$endtime\", $ULI)");
#            println("$commandstring")
            MySQL.mysql_execute(conn, commandstring)            
        end
    end
    MySQL.mysql_disconnect(conn)
end

database = "LocalWork"
table = "PumpTest"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))
print("Enter host name: ")
host = chomp(readline(STDIN))

# make backup file for the table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))

for s = 1:length(sheets)
    println("s=$s")
    try
        data = getdata(s)
	insertdata(data, s, host, username, password, database)
    catch
	warn("sheet $s failed")
    end
end



