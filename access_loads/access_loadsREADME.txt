for tables: GroundWaterLevelData, PortScrn, ScreenData
1. Check the sequence of the fields between the Access file and our tables first.(they are the same now 02/23/2017)

2. Export table as text file from Access database from Shannon('External Data' tab -> Export -> Text File), 
choose the directory(access_loads ) and give the right name(no spaces in the file name)-> ok
in 'Advanced' button:
* changing format of dates(date order) to "YMD" and 
* date delimiter to "-", 
* check "Leading Zeros in Dates"
->ok->finish

3. run script:
julia load_access_file.jl data_file db_table_name
where data_file is the text file exported from Access, db_table_name is the table that you are updating within the LocalWork database.
(load_access_file.jl will give the option to run clean_db.bat or not. clean_db.bat Deal with the naming difference between the MS Access database and LocalWork)

4. could use http://madsmax.lanl.gov/Wells/ to plot water level data to check the updated data in the database

NOTE:
Shannon does NOT maintain WQDBLocation any more, so we do NOT use this table from her database anymore.


Difference between tables of our database and Shannon's:
GroundWaterLevelData:our table has an extra key (rkey)
PortScrn: same
ScreenData:
	fields are the same  
	records: LocalWork has more records. 
	* when screen_id was changed to unique field, duplicates were found for record screen_id = 3721. The one that was the same as Shoannon's database was kept, the rest was changed to records screen_id 10000001~10000013
	* There are several other records with Screen_ID 5001~5005, 6202... which are not in Shannon's database, not sure about the data source of them, we just leave them as they are for now (2016/12/23)

* website to check "groundwater level data"
	http://madsmax.lanl.gov/Wells/
	1,2,3,4 are data from the table; 5,6 are about the plot
	
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
For "CdV-16-1(i)" Download water level ("Level Ft") from http://isi-data.com/
username: LANL_Guest  password:Password
--> "RDX Priority Monitoring"
use load_isi-data.jl to load the data into GroundWaterLevelData table
dataqualcode field need a empty string, and the probeno field need to be 0

# 7/17/2017
# Delete "MUL" key for LOCATION_NAME field from PortScrn table 07/17/2017
# backup file for the table /scratch/sft/ntao/mads_mysql/access_loads/archive/LocalWork_PortScrn_beforeDeleteMULkey.sql
# In /scratch/sft/ntao/mads_mysql/access_loads/load_access_file.jl, it is using "INSERT ON DUPLICATE KEY UPDATE"
# with LOCATION_NAME set as MUL key, when Shannon change the LOCATION_NAME of an old entry, 
# it looks like the new name does not updated into our table.

