# Deal with the naming difference for table GroundWaterLevelData, PortScrn and ScreenData between the MS Access database and LocalWork. 
# It is run every time LocalWork is updated with the MS Access database.

use LocalWork;
SET SQL_SAFE_UPDATES=0; #this has to be set for some of the following updates to work in mysql

# This section should be removed after the screen_name in wellname proble is solved
#UPDATE GroundWaterLevelData SET wellname = 'CDV-9-1(i)-PZ-1' WHERE wellname = 'CDV-9-1(i) PZ-1';
UPDATE GroundWaterLevelData SET wellname = 'CDV-9-1(i)-PZ-1' WHERE wellname = 'CDV-9-1(i) PZ1';
#UPDATE GroundWaterLevelData SET wellname = 'CDV-9-1(i)-PZ-2' WHERE wellname = 'CDV-9-1(i) PZ-2';
UPDATE GroundWaterLevelData SET wellname = 'CDV-9-1(i)-PZ-2' WHERE wellname = 'CDV-9-1(i) PZ2';
UPDATE GroundWaterLevelData SET wellname = 'CdV-16-4ip-S1' WHERE wellname = 'CdV-16-4ip S1';
#UPDATE ScreenData SET well_name = 'CDV-9-1(i)-PZ-1' WHERE well_name = 'CDV-9-1(i) PZ-1';
UPDATE ScreenData SET well_name = 'CDV-9-1(i)-PZ-1' WHERE well_name = 'CDV-9-1(i) PZ1';
#UPDATE ScreenData SET well_name = 'CDV-9-1(i)-PZ-2' WHERE well_name = 'CDV-9-1(i) PZ-2';
UPDATE ScreenData SET well_name = 'CDV-9-1(i)-PZ-2' WHERE well_name = 'CDV-9-1(i) PZ2';
UPDATE ScreenData SET well_name = 'CdV-16-4ip-S1' WHERE well_name = 'CdV-16-4ip S1';
#UPDATE PortScrn SET LOCATION_NAME = 'CDV-9-1(i)-PZ-1' WHERE LOCATION_NAME = 'CDV-9-1(i) PZ-1';
UPDATE PortScrn SET LOCATION_NAME = 'CDV-9-1(i)-PZ-1' WHERE LOCATION_NAME = 'CDV-9-1(i) PZ1';
#UPDATE PortScrn SET LOCATION_NAME = 'CDV-9-1(i)-PZ-2' WHERE LOCATION_NAME = 'CDV-9-1(i) PZ-2';
UPDATE PortScrn SET LOCATION_NAME = 'CDV-9-1(i)-PZ-2' WHERE LOCATION_NAME = 'CDV-9-1(i) PZ2';
UPDATE PortScrn SET LOCATION_NAME = 'CdV-16-4ip-S1' WHERE LOCATION_NAME = 'CdV-16-4ip S1';
########

UPDATE GroundWaterLevelData SET dataqualcode = 'Z216729' WHERE wellname REGEXP '^R-11$' AND time < 20050101;
UPDATE GroundWaterLevelData SET dataqualcode = 'Z216729' WHERE wellname REGEXP '^R-28$' AND time < 20050202;
UPDATE GroundWaterLevelData SET dataqualcode = 'Z216729' WHERE wellname REGEXP '^R-17$' AND portdesc REGEXP '1' AND time < 20060227;
UPDATE GroundWaterLevelData SET dataqualcode = 'Z216729' WHERE wellname REGEXP '^R-17$' AND portdesc REGEXP '2' and time < 20060227;
UPDATE GroundWaterLevelData SET dataqualcode = 'Z216729' WHERE wellname REGEXP '^R-24$' AND time < 20051202;
UPDATE GroundWaterLevelData SET dataqualcode = 'Z216729' WHERE wellname REGEXP '^R-10$' AND portdesc REGEXP '1' and time < 20051027;
UPDATE GroundWaterLevelData SET dataqualcode = 'Z216729' WHERE wellname REGEXP '^R-10a$' AND time < 20060404;
UPDATE GroundWaterLevelData SET dataqualcode = 'Z216729' WHERE wellname REGEXP '^R-33$' AND portdesc REGEXP '2' AND dataqualcode REGEXP 'V|VR| |VQ|VRVQ' AND time < 20061025;
UPDATE GroundWaterLevelData SET wellname = 'TW-01' WHERE wellname = 'Test Well 1';
UPDATE GroundWaterLevelData SET wellname = 'TW-01A' WHERE wellname = 'Test Well 1A';
UPDATE GroundWaterLevelData SET wellname = 'TW-02' WHERE wellname = 'Test Well 2';
UPDATE GroundWaterLevelData SET wellname = 'TW-02A' WHERE wellname = 'Test Well 2A';
UPDATE GroundWaterLevelData SET wellname = 'TW-03' WHERE wellname = 'Test Well 3'; 
UPDATE GroundWaterLevelData SET wellname = 'TW-04' WHERE wellname = 'Test Well 4';
UPDATE GroundWaterLevelData SET wellname = 'TW-08' WHERE wellname = 'Test Well 8';
UPDATE GroundWaterLevelData SET wellname = 'DT-10' WHERE wellname = 'Test Well DT-10';
UPDATE GroundWaterLevelData SET wellname = 'DT-05A' WHERE wellname = 'Test Well DT-5A';
UPDATE GroundWaterLevelData SET wellname = 'DT-09' WHERE wellname = 'Test Well DT-9';

UPDATE ScreenData SET WELL_NAME = 'TW-01' WHERE WELL_NAME = 'Test Well 1';
UPDATE ScreenData SET WELL_NAME = 'TW-01A' WHERE WELL_NAME = 'Test Well 1A';
UPDATE ScreenData SET WELL_NAME = 'TW-02' WHERE WELL_NAME = 'Test Well 2';
UPDATE ScreenData SET WELL_NAME = 'TW-02A' WHERE WELL_NAME = 'Test Well 2A';
UPDATE ScreenData SET WELL_NAME = 'TW-03' WHERE WELL_NAME = 'Test Well 3'; 
UPDATE ScreenData SET WELL_NAME = 'TW-04' WHERE WELL_NAME = 'Test Well 4';
UPDATE ScreenData SET WELL_NAME = 'TW-08' WHERE WELL_NAME = 'Test Well 8';
UPDATE ScreenData SET WELL_NAME = 'DT-10' WHERE WELL_NAME = 'Test Well DT-10';
UPDATE ScreenData SET WELL_NAME = 'DT-05A' WHERE WELL_NAME = 'Test Well DT-5A';
UPDATE ScreenData SET WELL_NAME = 'DT-09' WHERE WELL_NAME = 'Test Well DT-9';
UPDATE ScreenData SET WELL_NAME = 'B-01' WHERE WELL_NAME = 'Buckman 1';
UPDATE ScreenData SET WELL_NAME = 'B-02' WHERE WELL_NAME = 'Buckman 2';
UPDATE ScreenData SET WELL_NAME = 'B-03' WHERE WELL_NAME = 'Buckman 3';
UPDATE ScreenData SET WELL_NAME = 'B-03a' WHERE WELL_NAME = 'Buckman 3a';
UPDATE ScreenData SET WELL_NAME = 'B-04' WHERE WELL_NAME = 'Buckman 4';
UPDATE ScreenData SET WELL_NAME = 'B-05' WHERE WELL_NAME = 'Buckman 5';
UPDATE ScreenData SET WELL_NAME = 'B-06' WHERE WELL_NAME = 'Buckman 6';
UPDATE ScreenData SET WELL_NAME = 'B-07' WHERE WELL_NAME = 'Buckman 7';
UPDATE ScreenData SET WELL_NAME = 'B-08' WHERE WELL_NAME = 'Buckman 8';
UPDATE ScreenData SET WELL_NAME = 'B-09' WHERE WELL_NAME = 'Buckman 9';
UPDATE ScreenData SET WELL_NAME = 'B-10' WHERE WELL_NAME = 'Buckman 10';
UPDATE ScreenData SET WELL_NAME = 'B-11' WHERE WELL_NAME = 'Buckman 11';
UPDATE ScreenData SET WELL_NAME = 'B-12' WHERE WELL_NAME = 'Buckman 12';
UPDATE ScreenData SET WELL_NAME = 'B-13' WHERE WELL_NAME = 'Buckman 13';
#
UPDATE PortScrn SET LOCATION_NAME = 'TW-01' WHERE LOCATION_NAME = 'Test Well 1';
UPDATE PortScrn SET LOCATION_NAME = 'TW-01A' WHERE LOCATION_NAME = 'Test Well 1A';
UPDATE PortScrn SET LOCATION_NAME = 'TW-02' WHERE LOCATION_NAME = 'Test Well 2';
UPDATE PortScrn SET LOCATION_NAME = 'TW-02A' WHERE LOCATION_NAME = 'Test Well 2A';
UPDATE PortScrn SET LOCATION_NAME = 'TW-03' WHERE LOCATION_NAME = 'Test Well 3'; 
UPDATE PortScrn SET LOCATION_NAME = 'TW-04' WHERE LOCATION_NAME = 'Test Well 4';
UPDATE PortScrn SET LOCATION_NAME = 'TW-08' WHERE LOCATION_NAME = 'Test Well 8';
UPDATE PortScrn SET LOCATION_NAME = 'DT-10' WHERE LOCATION_NAME = 'Test Well DT-10';
UPDATE PortScrn SET LOCATION_NAME = 'DT-5A' WHERE LOCATION_NAME = 'Test Well DT-5A';
UPDATE PortScrn SET LOCATION_NAME = 'DT-9' WHERE LOCATION_NAME = 'Test Well DT-9';
UPDATE PortScrn SET LOCATION_NAME = 'B-01' WHERE LOCATION_NAME = 'Buckman 1';
UPDATE PortScrn SET LOCATION_NAME = 'B-02' WHERE LOCATION_NAME = 'Buckman 2';
UPDATE PortScrn SET LOCATION_NAME = 'B-03' WHERE LOCATION_NAME = 'Buckman 3';
UPDATE PortScrn SET LOCATION_NAME = 'B-03a' WHERE LOCATION_NAME = 'Buckman 3a';
UPDATE PortScrn SET LOCATION_NAME = 'B-04' WHERE LOCATION_NAME = 'Buckman 4';
UPDATE PortScrn SET LOCATION_NAME = 'B-05' WHERE LOCATION_NAME = 'Buckman 5';
UPDATE PortScrn SET LOCATION_NAME = 'B-06' WHERE LOCATION_NAME = 'Buckman 6';
UPDATE PortScrn SET LOCATION_NAME = 'B-07' WHERE LOCATION_NAME = 'Buckman 7';
UPDATE PortScrn SET LOCATION_NAME = 'B-08' WHERE LOCATION_NAME = 'Buckman 8';
UPDATE PortScrn SET LOCATION_NAME = 'B-09' WHERE LOCATION_NAME = 'Buckman 9';
UPDATE PortScrn SET LOCATION_NAME = 'B-10' WHERE LOCATION_NAME = 'Buckman 10';
UPDATE PortScrn SET LOCATION_NAME = 'B-11' WHERE LOCATION_NAME = 'Buckman 11';
UPDATE PortScrn SET LOCATION_NAME = 'B-12' WHERE LOCATION_NAME = 'Buckman 12';
UPDATE PortScrn SET LOCATION_NAME = 'B-13' WHERE LOCATION_NAME = 'Buckman 13';
#
# Add zero predeeding single digit in well names
#
UPDATE GroundWaterLevelData SET wellname = 'R-01' WHERE wellname = 'R-1';
UPDATE GroundWaterLevelData SET wellname = 'R-02' WHERE wellname = 'R-2';
UPDATE GroundWaterLevelData SET wellname = 'R-03i' WHERE wellname = 'R-3i';
UPDATE GroundWaterLevelData SET wellname = 'R-04' WHERE wellname = 'R-4';
UPDATE GroundWaterLevelData SET wellname = 'R-04 PZ-E' WHERE wellname = 'R-4 PZ-E';
UPDATE GroundWaterLevelData SET wellname = 'R-04 PZ-W' WHERE wellname = 'R-4 PZ-W';
UPDATE GroundWaterLevelData SET wellname = 'R-05' WHERE wellname = 'R-5';
UPDATE GroundWaterLevelData SET wellname = 'R-06' WHERE wellname = 'R-6';
UPDATE GroundWaterLevelData SET wellname = 'R-06i' WHERE wellname = 'R-6i';
UPDATE GroundWaterLevelData SET wellname = 'R-07' WHERE wellname = 'R-7';
UPDATE GroundWaterLevelData SET wellname = 'R-08' WHERE wellname = 'R-8';
UPDATE GroundWaterLevelData SET wellname = 'R-09' WHERE wellname = 'R-9';
UPDATE GroundWaterLevelData SET wellname = 'R-09i' WHERE wellname = 'R-9i';
UPDATE GroundWaterLevelData SET wellname = 'G-01A' WHERE wellname = 'G-1A';
UPDATE GroundWaterLevelData SET wellname = 'G-02A' WHERE wellname = 'G-2A';
UPDATE GroundWaterLevelData SET wellname = 'G-03' WHERE wellname = 'G-3';
UPDATE GroundWaterLevelData SET wellname = 'G-03A' WHERE wellname = 'G-3A';
UPDATE GroundWaterLevelData SET wellname = 'G-04' WHERE wellname = 'G-4';
UPDATE GroundWaterLevelData SET wellname = 'G-04A' WHERE wellname = 'G-4A';
UPDATE GroundWaterLevelData SET wellname = 'G-05A' WHERE wellname = 'G-5A';
UPDATE GroundWaterLevelData SET wellname = 'O-01' WHERE wellname = 'O-1';
UPDATE GroundWaterLevelData SET wellname = 'O-04' WHERE wellname = 'O-4';
UPDATE GroundWaterLevelData SET wellname = 'PM-01' WHERE wellname = 'PM-1';
UPDATE GroundWaterLevelData SET wellname = 'PM-02' WHERE wellname = 'PM-2';
UPDATE GroundWaterLevelData SET wellname = 'PM-03' WHERE wellname = 'PM-3';
UPDATE GroundWaterLevelData SET wellname = 'PM-04' WHERE wellname = 'PM-4';
UPDATE GroundWaterLevelData SET wellname = 'PM-05' WHERE wellname = 'PM-5';

UPDATE PortScrn SET LOCATION_NAME = 'R-01' WHERE LOCATION_NAME = 'R-1';
UPDATE PortScrn SET LOCATION_NAME = 'R-02' WHERE LOCATION_NAME = 'R-2';
UPDATE PortScrn SET LOCATION_NAME = 'R-03' WHERE LOCATION_NAME = 'R-3';
UPDATE PortScrn SET LOCATION_NAME = 'R-03i' WHERE LOCATION_NAME = 'R-3i';
UPDATE PortScrn SET LOCATION_NAME = 'R-04' WHERE LOCATION_NAME = 'R-4';
UPDATE PortScrn SET LOCATION_NAME = 'R-04 PZ-E' WHERE LOCATION_NAME = 'R-4 PZ-E';
UPDATE PortScrn SET LOCATION_NAME = 'R-04 PZ-W' WHERE LOCATION_NAME = 'R-4 PZ-W';
UPDATE PortScrn SET LOCATION_NAME = 'R-05' WHERE LOCATION_NAME = 'R-5';
UPDATE PortScrn SET LOCATION_NAME = 'R-06' WHERE LOCATION_NAME = 'R-6';
UPDATE PortScrn SET LOCATION_NAME = 'R-06i' WHERE LOCATION_NAME = 'R-6i';
UPDATE PortScrn SET LOCATION_NAME = 'R-07' WHERE LOCATION_NAME = 'R-7';
UPDATE PortScrn SET LOCATION_NAME = 'R-08' WHERE LOCATION_NAME = 'R-8';
UPDATE PortScrn SET LOCATION_NAME = 'R-08A' WHERE LOCATION_NAME = 'R-8A';
UPDATE PortScrn SET LOCATION_NAME = 'R-09' WHERE LOCATION_NAME = 'R-9';
UPDATE PortScrn SET LOCATION_NAME = 'R-09i' WHERE LOCATION_NAME = 'R-9i';
UPDATE PortScrn SET LOCATION_NAME = 'G-01A' WHERE LOCATION_NAME = 'G-1A';
UPDATE PortScrn SET LOCATION_NAME = 'G-02A' WHERE LOCATION_NAME = 'G-2A';
UPDATE PortScrn SET LOCATION_NAME = 'G-03' WHERE LOCATION_NAME = 'G-3';
UPDATE PortScrn SET LOCATION_NAME = 'G-03A' WHERE LOCATION_NAME = 'G-3A';
UPDATE PortScrn SET LOCATION_NAME = 'G-04' WHERE LOCATION_NAME = 'G-4';
UPDATE PortScrn SET LOCATION_NAME = 'G-04A' WHERE LOCATION_NAME = 'G-4A';
UPDATE PortScrn SET LOCATION_NAME = 'G-05A' WHERE LOCATION_NAME = 'G-5A';
UPDATE PortScrn SET LOCATION_NAME = 'O-01' WHERE LOCATION_NAME = 'O-1';
UPDATE PortScrn SET LOCATION_NAME = 'O-04' WHERE LOCATION_NAME = 'O-4';
UPDATE PortScrn SET LOCATION_NAME = 'PM-01' WHERE LOCATION_NAME = 'PM-1';
UPDATE PortScrn SET LOCATION_NAME = 'PM-02' WHERE LOCATION_NAME = 'PM-2';
UPDATE PortScrn SET LOCATION_NAME = 'PM-03' WHERE LOCATION_NAME = 'PM-3';
UPDATE PortScrn SET LOCATION_NAME = 'PM-04' WHERE LOCATION_NAME = 'PM-4';
UPDATE PortScrn SET LOCATION_NAME = 'PM-05' WHERE LOCATION_NAME = 'PM-5';

UPDATE ScreenData SET WELL_NAME = 'R-01' WHERE WELL_NAME = 'R-1';
UPDATE ScreenData SET WELL_NAME = 'R-02' WHERE WELL_NAME = 'R-2';
UPDATE ScreenData SET WELL_NAME = 'R-03i' WHERE WELL_NAME = 'R-3i';
UPDATE ScreenData SET WELL_NAME = 'R-04' WHERE WELL_NAME = 'R-4';
UPDATE ScreenData SET WELL_NAME = 'R-04 PZ-E' WHERE WELL_NAME = 'R-4 PZ-E';
UPDATE ScreenData SET WELL_NAME = 'R-04 PZ-W' WHERE WELL_NAME = 'R-4 PZ-W';
UPDATE ScreenData SET WELL_NAME = 'R-05' WHERE WELL_NAME = 'R-5';
UPDATE ScreenData SET WELL_NAME = 'R-06' WHERE WELL_NAME = 'R-6';
UPDATE ScreenData SET WELL_NAME = 'R-06i' WHERE WELL_NAME = 'R-6i';
UPDATE ScreenData SET WELL_NAME = 'R-07' WHERE WELL_NAME = 'R-7';
UPDATE ScreenData SET WELL_NAME = 'R-08' WHERE WELL_NAME = 'R-8';
UPDATE ScreenData SET WELL_NAME = 'R-09' WHERE WELL_NAME = 'R-9';
UPDATE ScreenData SET WELL_NAME = 'R-09i' WHERE WELL_NAME = 'R-9i';
UPDATE ScreenData SET WELL_NAME = 'G-01A' WHERE WELL_NAME = 'G-1A';
UPDATE ScreenData SET WELL_NAME = 'G-02A' WHERE WELL_NAME = 'G-2A';
UPDATE ScreenData SET WELL_NAME = 'G-03' WHERE WELL_NAME = 'G-3';
UPDATE ScreenData SET WELL_NAME = 'G-03A' WHERE WELL_NAME = 'G-3A';
UPDATE ScreenData SET WELL_NAME = 'G-04' WHERE WELL_NAME = 'G-4';
UPDATE ScreenData SET WELL_NAME = 'G-04A' WHERE WELL_NAME = 'G-4A';
UPDATE ScreenData SET WELL_NAME = 'G-05A' WHERE WELL_NAME = 'G-5A';
UPDATE ScreenData SET WELL_NAME = 'O-01' WHERE WELL_NAME = 'O-1';
UPDATE ScreenData SET WELL_NAME = 'O-04' WHERE WELL_NAME = 'O-4';
UPDATE ScreenData SET WELL_NAME = 'PM-01' WHERE WELL_NAME = 'PM-1';
UPDATE ScreenData SET WELL_NAME = 'PM-02' WHERE WELL_NAME = 'PM-2';
UPDATE ScreenData SET WELL_NAME = 'PM-03' WHERE WELL_NAME = 'PM-3';
UPDATE ScreenData SET WELL_NAME = 'PM-04' WHERE WELL_NAME = 'PM-4';
UPDATE ScreenData SET WELL_NAME = 'PM-05' WHERE WELL_NAME = 'PM-5';

# moved updating ProdDaily part to clean_dbProdDaily.bat
#
# Turn off records as per Monty email 20080516
#
UPDATE GroundWaterLevelData SET dataqualcode = 'Z216729' WHERE wellname REGEXP '^R-15$' AND time <= '2004-11-19 23:59:59';
#

SET SQL_SAFE_UPDATES=1;
