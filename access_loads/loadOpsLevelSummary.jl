# load Depth as water level in this script 

import ExcelReaders
import DataFrames
import DataStructures
import MySQL

file = ARGS[1]
table = "GroundWaterLevelData"
database = "LocalWork"
host = "mads01"

function insertdata(data, host, username, password, database, table, wellname)
    conn = MySQL.mysql_connect(host, username, password, database)
    
    for i = 1:size(data,1) #size(data,1) returns the number of rows in $data
        time = Dates.format(data[1][i],"yyyy-mm-dd HH:MM:SS")
        depth = data[3][i]
        if depth > 0
            commandstring = string("insert into $table (wellname, time, piezometricWLft) values (\"$wellname\", \"$time\", $depth)");
            #println("$commandstring")
            MySQL.mysql_execute(conn, commandstring)
        elseif depth < 0
            println("$wellname time depth!")
        end
    end
    
end

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))

# make backup file for $table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))

#wells = ["CrEX-2", "CrEX-3", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5"]
# CrEX-1 data in OpsLevelSummary_1-25-18 was already in our database

wellname = "CrEX-2"
data = ExcelReaders.readxl(DataFrames.DataFrame, file, "Level Data!G2:I3525", header=false)
insertdata(data, host, username, password, database, table, wellname)

wellname = "CrEX-3"
data = ExcelReaders.readxl(DataFrames.DataFrame, file, "Level Data!M2:O422", header=false)
insertdata(data, host, username, password, database, table, wellname)

wellname = "CrIN-1"
data = ExcelReaders.readxl(DataFrames.DataFrame, file, "Level Data!S2:U142", header=false)
insertdata(data, host, username, password, database, table, wellname)

wellname = "CrIN-2"
data = ExcelReaders.readxl(DataFrames.DataFrame, file, "Level Data!Y2:AA140", header=false)
insertdata(data, host, username, password, database, table, wellname)

wellname = "CrIN-3"
data = ExcelReaders.readxl(DataFrames.DataFrame, file, "Level Data!AE2:AG160", header=false)
insertdata(data, host, username, password, database, table, wellname)

wellname = "CrIN-4"
data = ExcelReaders.readxl(DataFrames.DataFrame, file, "Level Data!AK2:AM996", header=false)
insertdata(data, host, username, password, database, table, wellname)

wellname = "CrIN-5"
data = ExcelReaders.readxl(DataFrames.DataFrame, file, "Level Data!AQ2:AS252", header=false)
insertdata(data, host, username, password, database, table, wellname)
