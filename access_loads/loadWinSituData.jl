# loadWinSituData.jl load "Temperature", "Calculated Groundwater Elevation" data
# from 20180124SupplyWellData012418revised to GroundWaterLevelData table
# julia load_supplyWellData.jl inputfile

import MySQL
import ExcelReaders
import DataFrames
#sheets = ["G-1A","G-2A","G-3A","G-4A","G-5A","O-1","O-4","PM-1","PM-2new","PM-3","PM-4","PM-5"]
#sheets = ["G-2A","G-3A","G-4A","G-5A","O-1","PM-1","PM-2","PM-3","PM-4","PM-5"]
sheets = ["GW2A","GW3A","GW4A","GW5A","OW1","OW4","PW4","PW5"]
startRowInSheet = [9010,8991,3079,3081,9009,1413,2977,18552]
lastRowInSheet = [9654,9635,3723,3724,9607,2016,3575,19150]
#startRowInSheet = [7517,7498,1587,1588,7477,73,1442,17018] # 03132018
#lastRowInSheet = [9008,8989,3077,3079,9007,1411,2975,18550] # 03132018
#sheets = ["G-1A","O-4"]
#lastRowInSheet = [33887,29696]

function get_insert_data(host, username, password, database, file, table, sheetNum, startRow=startRowInSheet[sheetNum], endRow=lastRowInSheet[sheetNum])
    data = ExcelReaders.readxl(DataFrames.DataFrame, file, "$(sheets[sheetNum])!A$startRow:H$endRow", header=false)
    conn = MySQL.mysql_connect(host, username, password, database)
    for i = 1:size(data,1) #size(data,1) returns the number of rows in $data
        wellname = data[1][i]
        time = Dates.format(data[2][i],"yyyy-mm-dd HH:MM:SS")
        #pressurepsia = data[4][i]
        temperatureC = data[8][i]
        piezometricWLft = data[7][i]
        commandstring = string("insert into $table (wellname, time, temperatureC, piezometricWLft) values (\"$wellname\", \"$time\", $temperatureC, $piezometricWLft)");
        #println(commandstring)
        MySQL.mysql_execute(conn, commandstring)
    end
    MySQL.mysql_disconnect(conn)
end


if (length(ARGS) != 1)
    println("Usage:\njulia loadWinSituData.jl input_file")
    exit(0)
end
file = ARGS[1]
#table = "tGroundWaterLevelData"
table = "GroundWaterLevelData"
database = "LocalWork"
host = "mads01"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))

# make backup file for $table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))

for s = 1:length(sheets)
    println("s=$s")
    try
        get_insert_data(host, username, password, database, file, table, s)
    catch
	warn("sheet $s failed")
    end
end
