# load_PM1_PM2_PM3.jl load "Temperature", "Calculated Groundwater Elevation" data
# from 20180320PM1_PM2_PM3_0318.xlsx to GroundWaterLevelData table


import MySQL
import ExcelReaders
import DataFrames

sheets = ["PM-1","PM-2","PM-3"]
startRowInSheet = [14936,1373,1373]
lastRowInSheet = [16467,2906,2906]

function get_insert_data(host, username, password, database, file, table, sheetNum, startRow=startRowInSheet[sheetNum], endRow=lastRowInSheet[sheetNum])
    data = ExcelReaders.readxl(DataFrames.DataFrame, file, "$(sheets[sheetNum])!A$startRow:I$endRow", header=false)
    conn = MySQL.mysql_connect(host, username, password, database)
    for i = 1:size(data,1) #size(data,1) returns the number of rows in $data
        wellname = data[1][i]
        time = Dates.format(data[2][i],"yyyy-mm-dd HH:MM:SS")
        #pressurepsia = data[4][i]
        temperatureC = data[9][i]
        piezometricWLft = data[8][i]
        commandstring = string("insert into $table (wellname, time, temperatureC, piezometricWLft) values (\"$wellname\", \"$time\", $temperatureC, $piezometricWLft)");
        #println(commandstring)
        MySQL.mysql_execute(conn, commandstring)
    end
    MySQL.mysql_disconnect(conn)
end


if (length(ARGS) != 1)
    println("Usage:\njulia load_PM1_PM2_PM3.jl input_file")
    exit(0)
end
file = ARGS[1]
#table = "tGroundWaterLevelData"
table = "GroundWaterLevelData"
database = "LocalWork"
host = "mads01"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))

# make backup file for $table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))

for s = 1:length(sheets)
    println("s=$s")
    try
        get_insert_data(host, username, password, database, file, table, s)
    catch
	warn("sheet $s failed")
    end
end
