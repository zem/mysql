# load_access_file.jl
# Update LocalWork (table: GroundWaterLevelData, ScreenData, PortScrn) with the MS Access database.
#
# Usage:
# julia load_access_file.jl data_file db_table_name
#
# data_file : is the file that contains data that needs to be load to the database
# db_table_name : the name of the table where to load this data
#
# NOTE:
# When there are updates(entries with the same value for the unique field) in the
# input file for the entries that are already in the database,
# then this script updates the table with the new values from the input file.
#
# In one input file, if there are entries with the same value for the 'unique' field,
# LOAD DATA LOCAL INFILE use the first entry and ignores the rest. It is used here
# this way since we do not expect duplicates or updates in the same input file. 


import MySQL
import DataFrames


# the table PortScrn in Shannon's database has an extra field than our table, LOAD DATA LOCAL INFILE
# in this function just ignor the extra field if it is included in the input file
function updateData(host, username, password, database, file, table)
    #have to use 'MYSQL_OPT_LOCAL_INFILE=>1' while connecting so the following LOAD DATA LOCAL INFILE will work
    conn = MySQL.mysql_connect(host, username, password, database, opts = Dict(MySQL.MYSQL_OPT_LOCAL_INFILE=>1))

    columns = MySQL.mysql_execute(conn, "SHOW COLUMNS FROM $table;")[1]
    
    MySQL.mysql_execute(conn, "create table temp_table like $table;")

    # 'LOAD DATA LOCAL INFILE' load empty field as 0 into the table. Using 'nullif' makes sure empty field loads
    # as NULL
    command = string("LOAD DATA LOCAL INFILE '$file' INTO TABLE temp_table FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' \(")
    for c = 1:size(columns,1)
        column = columns[1][c]
        if c == size(columns,1) #last column
            command = command * "@v$column"
        else
            command = command * "@v$column, "
        end
    end
    command = command * "\) SET "
    for c = 1:size(columns,1)
        column = columns[1][c]
        if c == size(columns,1) #last column
            command = command * "$column = nullif\(@v$column,''\);"
        else
            command = command * "$column = nullif\(@v$column,''\), "
        end
    end
    #@show command
    #println(command)
    MySQL.mysql_execute(conn, command)


    command = string("INSERT INTO $table SELECT * FROM temp_table ON DUPLICATE KEY UPDATE")	      
    
    for c = 1:size(columns,1)
        column = columns[1][c]
        if c == size(columns,1) #last column
            command = command * " $column = VALUES($column);"
        else
            command = command * " $column = VALUES($column),"
        end
    end
#    println(command)
    MySQL.mysql_execute(conn, command)
    MySQL.mysql_execute(conn, "DROP TABLE temp_table;")

    MySQL.mysql_disconnect(conn)
end


# This function is not used in the updating process right now, since we decide to NOT update 'rkey' field
# in our GroundWaterLevelData table anymore. But it could be used in other updating process for similar situation
#
# To update a table which has one more column (also a auto_increment primary key) than
# the data source table, with a txt/csv file 
function updateDataExtraKey(host, username, password, database, file, table)
    println("Loading data...")
    #have to use 'MYSQL_OPT_LOCAL_INFILE=>1' while connecting so the following LOAD DATA LOCAL INFILE will work
    conn = MySQL.mysql_connect(host, username, password, database, opts = Dict(MySQL.MYSQL_OPT_LOCAL_INFILE=>1))
    primaryKey = MySQL.mysql_execute(conn, "show index from $table where Key_name = 'PRIMARY';")
    primaryKeyName = primaryKey[5][1]
    println("primary key:", primaryKeyName)
    command = string("SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS where table_name = '$table' and column_name = '$primaryKeyName';")
    ktype = MySQL.mysql_execute(conn, command)
    MySQL.mysql_execute(conn, "ALTER TABLE $table DROP PRIMARY KEY, CHANGE $primaryKeyName $primaryKeyName $(ktype[1][1]);")
    MySQL.mysql_execute(conn, "create table temp_table like $table;")
    #NOTE: do not drop the extra column here, since the 'column count' need to match for ON DUPLICATE KEY UPDATE

    # 'LOAD DATA LOCAL INFILE' load empty field as 0 into the table. Using 'nullif' makes sure empty field loads
    # as NULL
    # the 'nullif' part need to be added to this function too (refer to updateData function about how to do it)
    command = string("LOAD DATA LOCAL INFILE '$file' INTO TABLE temp_table FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"';")
#    println(command)
    MySQL.mysql_execute(conn, command)
    #Now temp_table has all the data from the input file, with NULL for the extra field of all the entries    

    command = string("INSERT INTO $table SELECT * FROM temp_table ON DUPLICATE KEY UPDATE")	      
    columns = MySQL.mysql_execute(conn, "SHOW COLUMNS FROM $table;")[1]
    for c = 1:size(columns,1)
        column = columns[1][c]
        if c == size(columns,1) #last column
            command = command * " $column = VALUES($column);"
        else
            command = command * " $column = VALUES($column),"
        end
    end
    #println(command)
    MySQL.mysql_execute(conn, command)

    MySQL.mysql_execute(conn, "Alter table $table modify $primaryKeyName $(ktype[1][1]) AUTO_INCREMENT PRIMARY KEY;")
    MySQL.mysql_execute(conn, "DROP TABLE temp_table;")
    MySQL.mysql_disconnect(conn)
end


# While exporting table as text file from Access database, if the format of dates is not changed to "YMD",
# the wrong formated dates in result data file will be 'out of range dates' and loaded into GroundWaterLevelData
# as '0000-00-00 00:00:00' when using 'LOAD DATA LOCAL INFILE', since the datatype of field 'time' in
# GroundWaterLevelData table is 'datetime'.
#
# checkDateFormat is trying to check the date format by loading the whole data file into a dataframe and then try
# to assign the time data to a DateTime array of the correct formated date. When the time data is not in the correct
# format, julia will throw an error saying "the argument is out of range ..."
# In this case, the updateData function is NOT called, so the bad formated data will NOT load into GroundWaterLevelData
# and a WARNING message about it is printed. 
function checkDateFormat(file)
    #f = DataFrames.DataFrame(id=Int[], wellname=String[], time=String[], pressurepsia=Float64[], temperatureC=Float64[], atmosphericpsia=Float64[], piezometricWLft=Float64[], MPHTft=Float64[], MDTWft=Float64[], probeno=Float64[], SNTransducer=String[], portdesc=String[], portdepthft=Float64[], WLmethodcode=String[], WLtypecode=String[], comments=String[], dataqualcode=String[], validreasoncode=String[], sourcefile=String[])
    #f = DataFrames.readtable(file,header=["id","wellname","time","pressurepsia","temperatureC","atmosphericpsia","piezometricWLft","MPHTft","MDTWft","probeno","SNTransducer","portdesc","portdepthft","WLmethodcode","WLtypecode","comments","dataqualcode","validreasoncode","sourcefile"])
    f = DataFrames.readtable(file,header=false)
    #@show f
    outOfRangeDate = DateTime[]
    outOfRangeDate = DateTime[DateTime(d, "yyyy-mm-dd HH:MM:SS") for d in f[3]]
    #@show outOfRangeDate
end


if (length(ARGS) != 2)
    println("Usage:\njulia load_access_file.jl data_file db_table_name")
    exit(0)
end
file = ARGS[1]
table = ARGS[2]
database = "LocalWork"


# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))
print("Enter host name: ")
host = chomp(readline(STDIN))

# ask user about running clean_db.bat
println("\nclean_db.bat take care of the name difference between the input data and our database.")
print("Do you want to run clean_db.bat, which could take more than an hour to run(y/n)?  ")
clean_db = chomp(readline(STDIN))
if (clean_db == "y")
    println("Will run clean_db after loading the data")
else
    println("Will NOT run clean_db")
end


run(`dos2unix $file`)

# make backup file for $table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))


#if (table == "ScreenData" || table == "PortScrn" || table == "GroundWaterLevelData")
if (table == "ScreenData" || table == "PortScrn" || table == "test4" || table == "tPortScrn" || table == "tScreenData")
    #GroundWaterLevelData is big and takes too long to alter the primary key, so just use this function to import the data
    #and leave the field rkey as NULL for now
    updateData(host, username, password, database, file, table)
elseif (table == "GroundWaterLevelData" || table == "t2GroundWaterLevelData")
    try
	checkDateFormat(file)
        updateData(host, username, password, database, file, table)
        println("Finish updating the table with input file")
    catch e
	warn("there was an error with the data: $e\ndata not updated into $table")
    end 
elseif (table == "test1") 
    updateDataExtraKey(host, username, password, database, file, table)
    println("Finish updating the table with input file")
else
    println("This script does not update table \"$table\"")
end

if (clean_db == "y")
    println("running clean_db.bat ...")
    #can also use the following command to run clean_db.bat at unix prompt
    #mysql -p < clean_db.bat
    run(pipeline(`cat clean_db.bat`,`mysql -u $username -p$password LocalWork`))
end
