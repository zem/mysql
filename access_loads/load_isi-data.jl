# load_isi-data.jl load "Level Ft" data from isi-data.com to GroundWaterLevelData table
# save data into a excel file, then run the following
# julia load_isi-data.jl file

import MySQL
import ExcelReaders
import DataFrames
sheets = ["CdV-16-1(i)"]

function get_insert_data(host, username, password, database, file, table, startRow=3, endRow=948)
    data = ExcelReaders.readxl(DataFrames.DataFrame, file, "$(sheets[1])!A$startRow:B$endRow", header=false)
    conn = MySQL.mysql_connect(host, username, password, database)
    for i = 1:size(data,1) #size(data,1) returns the number of rows in $data
        wellname = "CdV-16-1(i)"
        time = Dates.format(data[1][i],"yyyy-mm-dd HH:MM:SS")
        piezometricWLft = data[2][i]
        commandstring = string("insert into $table (wellname, time, piezometricWLft) values (\"$wellname\", \"$time\", $piezometricWLft)");
        println(commandstring)
        MySQL.mysql_execute(conn, commandstring)
    end
    MySQL.mysql_disconnect(conn)
end


if (length(ARGS) != 1)
    println("Usage:\njulia load_isi-data.jl input_file")
    exit(0)
end
file = ARGS[1]
table = "GroundWaterLevelData"
database = "LocalWork"
host = "madsmax"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))

# make backup file for $table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))

get_insert_data(host, username, password, database, file, table)
