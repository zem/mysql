# load_supplyWellData.jl load "Temperature", "Calculated Groundwater Elevation" data
# from 20180124SupplyWellData012418revised to GroundWaterLevelData table
# julia load_supplyWellData.jl inputfile

import MySQL
import ExcelReaders
import DataFrames
#sheets = ["G-1A","G-2A","G-3A","G-4A","G-5A","O-1","O-4","PM-1","PM-2new","PM-3","PM-4","PM-5"]
sheets = ["G-2A","G-3A","G-4A","G-5A","O-1","PM-1","PM-2","PM-3","PM-4","PM-5"]
startRowInSheet = [5228,5209,2,12771,5189,12052,10226,15437,3775,2]
lastRowInSheet = [7435,7416,2208,14979,7396,14933,13103,18315,5832,2879]
#sheets = ["G-1A","O-4"]
#lastRowInSheet = [33887,29696]

function get_insert_data(host, username, password, database, file, table, sheetNum, startRow=startRowInSheet[sheetNum], endRow=lastRowInSheet[sheetNum])
    if sheets[sheetNum] == "G-1A" || sheets[sheetNum] == "O-4"
        data = ExcelReaders.readxl(DataFrames.DataFrame, file, "$(sheets[sheetNum])!A$startRow:D$endRow", header=false)
        conn = MySQL.mysql_connect(host, username, password, database)
        for i = 1:size(data,1) #size(data,1) returns the number of rows in $data
            wellname = data[1][i]
            time = Dates.format(data[2][i],"yyyy-mm-dd HH:MM:SS")
            pressurepsia = data[3][i]
            piezometricWLft = data[4][i]
            commandstring = string("insert into $table (wellname, time, pressurepsia, piezometricWLft) values (\"$wellname\", \"$time\", $pressurepsia, $piezometricWLft)");
            #println(commandstring)
            MySQL.mysql_execute(conn, commandstring)
        end
        MySQL.mysql_disconnect(conn)
    else
        data = ExcelReaders.readxl(DataFrames.DataFrame, file, "$(sheets[sheetNum])!A$startRow:D$endRow", header=false)
        conn = MySQL.mysql_connect(host, username, password, database)
        for i = 1:size(data,1) #size(data,1) returns the number of rows in $data
            wellname = data[1][i]
            time = Dates.format(data[2][i],"yyyy-mm-dd HH:MM:SS")
            #pressurepsia = data[3][i]
            temperatureC = data[3][i]
            piezometricWLft = data[4][i]
            commandstring = string("insert into $table (wellname, time, temperatureC, piezometricWLft) values (\"$wellname\", \"$time\", $temperatureC, $piezometricWLft)");
            #println(commandstring)
            MySQL.mysql_execute(conn, commandstring)
        end
        MySQL.mysql_disconnect(conn)
    end
end


if (length(ARGS) != 1)
    println("Usage:\njulia load_supplyWellData.jl input_file")
    exit(0)
end
file = ARGS[1]
#table = "t1GroundWaterLevelData"
table = "GroundWaterLevelData"
database = "LocalWork"
host = "mads01"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))

# make backup file for $table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))

for s = 1:length(sheets)
    println("s=$s")
    try
        get_insert_data(host, username, password, database, file, table, s)
    catch
	warn("sheet $s failed")
    end
end
