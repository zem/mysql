use LocalWork;
SET SQL_SAFE_UPDATES=0; #this has to be set for some of the following updates to work in mysql

UPDATE ProdDaily SET wellname = 'G-01' WHERE wellname = 'G-1';
UPDATE ProdDaily SET wellname = 'G-01A' WHERE wellname = 'G-1A';
UPDATE ProdDaily SET wellname = 'G-02' WHERE wellname = 'G-2';
UPDATE ProdDaily SET wellname = 'G-02A' WHERE wellname = 'G-2A';
UPDATE ProdDaily SET wellname = 'G-03A' WHERE wellname = 'G-3A';
UPDATE ProdDaily SET wellname = 'G-04' WHERE wellname = 'G-4';
UPDATE ProdDaily SET wellname = 'G-04A' WHERE wellname = 'G-4A';
UPDATE ProdDaily SET wellname = 'G-05' WHERE wellname = 'G-5';
UPDATE ProdDaily SET wellname = 'G-05A' WHERE wellname = 'G-5A';
UPDATE ProdDaily SET wellname = 'G-06' WHERE wellname = 'G-6';
UPDATE ProdDaily SET wellname = 'O-01' WHERE wellname = 'O-1';
UPDATE ProdDaily SET wellname = 'O-04' WHERE wellname = 'O-4';
UPDATE ProdDaily SET wellname = 'B-01' WHERE wellname = 'Buckman 1';
UPDATE ProdDaily SET wellname = 'B-02' WHERE wellname = 'Buckman 2';
UPDATE ProdDaily SET wellname = 'B-03' WHERE wellname = 'Buckman 3';
UPDATE ProdDaily SET wellname = 'B-04' WHERE wellname = 'Buckman 4';
UPDATE ProdDaily SET wellname = 'B-05' WHERE wellname = 'Buckman 5';
UPDATE ProdDaily SET wellname = 'B-06' WHERE wellname = 'Buckman 6';
UPDATE ProdDaily SET wellname = 'B-07' WHERE wellname = 'Buckman 7';
UPDATE ProdDaily SET wellname = 'B-08' WHERE wellname = 'Buckman 8';
UPDATE ProdDaily SET wellname = 'B-09' WHERE wellname = 'Buckman 9';
UPDATE ProdDaily SET wellname = 'B-10' WHERE wellname = 'Buckman 10';
UPDATE ProdDaily SET wellname = 'B-11' WHERE wellname = 'Buckman 11';
UPDATE ProdDaily SET wellname = 'B-12' WHERE wellname = 'Buckman 12';
UPDATE ProdDaily SET wellname = 'B-13' WHERE wellname = 'Buckman 13';
UPDATE ProdDaily SET wellname = 'PM-01' WHERE wellname = 'PM-1';
UPDATE ProdDaily SET wellname = 'PM-02' WHERE wellname = 'PM-2';
UPDATE ProdDaily SET wellname = 'PM-03' WHERE wellname = 'PM-3';
UPDATE ProdDaily SET wellname = 'PM-04' WHERE wellname = 'PM-4';
UPDATE ProdDaily SET wellname = 'PM-05' WHERE wellname = 'PM-5';

#The following line may need to be uncommented if ProdDaily has been reloaded
#
#ALTER TABLE ProdDaily ADD COLUMN (dataqualcode VARCHAR(30));
UPDATE ProdDaily SET dataqualcode = 'Z216729' WHERE wellname REGEXP '"O-01"' AND time >= '2006-01-12' and time <= '2006-01-20';

SET SQL_SAFE_UPDATES=1;