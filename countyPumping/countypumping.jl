# This script load county pumping data into table ProdDaily of LocalWork
# Usage:
# julia countypumping.jl input_file
#
# Negative values:
# There are negative values for "pumping" in the data file which are caculated from daily reading values.
# This script changes the negative pumping to 0 and subtract the absolute value from the next day's pumping.
# 
# There are negative values for "runtime" too, which are caculated from "pumping". This script does NOT
# change these now since runtime does not matter too much for now.

import ExcelReaders
import DataFrames
import DataStructures
import MySQL


function getdata(monthnum, file, startday=1, endday=daysinmonth[monthnum])
    pumping = ExcelReaders.readxl(DataFrames.DataFrame, file, "$(months[monthnum])!A$(40+startday-1):M$(40+endday-1)", header=false, colnames=map(Symbol, ["Date", "G-01A", "G-02A", "G-03A", "G-04A", "G-05A", "O-01", "O-04", "PM-01", "PM-02", "PM-03", "PM-04", "PM-05"]))
    runtime = ExcelReaders.readxl(DataFrames.DataFrame, file, "$(months[monthnum])!A$(75+startday-1):M$(75+endday-1)", header=false, colnames=map(Symbol, ["Date", "G-01A", "G-02A", "G-03A", "G-04A", "G-05A", "O-01", "O-04", "PM-01", "PM-02", "PM-03", "PM-04", "PM-05"]))
    return pumping, runtime
end

function insertdata(pumping, runtimes, host, username, password, database, file, table, negativePumping, force=false)
    conn = MySQL.mysql_connect(host, username, password, database)
    maxtime = MySQL.mysql_execute(conn, "select max(time) from $table;")[1][1]
    tmpmin=minimum(pumping[:Date])

    if (maxtime >= minimum(pumping[:Date]))
	if force
	    warn("This data may already be in the database.")
	else
	    MySQL.mysql_disconnect(conn)
	    error("This data may already be in the database.")
	end
    end
    
    for wellname in filter(x->x != :Date, names(pumping))
	for i = 1:size(pumping, 1)
	    dtstring = Dates.format(pumping[:Date][i], "yyyy-mm-dd HH:MM:SS")
	    runtimehr = runtimes[wellname][i]
	    productiongal = pumping[wellname][i]

            # deal with negative value for pumping
            if (negativePumping[wellname][1] < 0)
                productiongal = productiongal + negativePumping[wellname][1]
                negativePumping[wellname][1] = 0
            end
            if (productiongal < 0)
                if (productiongal < -100000)
                    warn("\nproductiongal=$productiongal on $dtstring\n")
                end
                negativePumping[wellname][1] = productiongal
                productiongal = 0
            end

            string_wellname = string(wellname)
            commandstring = string("insert into $table (time,wellname,runtimehr,productiongal) values (\"$dtstring\", \"$string_wellname\", $runtimehr, $productiongal);")
            MySQL.mysql_execute(conn, commandstring)
	end
    end 
    MySQL.mysql_disconnect(conn)
    return negativePumping
end


if (length(ARGS) != 1)
    println("Usage:\njulia countypumping.jl input_file")
    exit(0)
end
file = ARGS[1]
table = "ProdDaily"
database = "LocalWork"
#host = "madsmax"

# check for the number of days of Feb for the year of interest
months = ["JAN", "FEB", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC"]
daysinmonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))
print("Enter host name: ")
host = chomp(readline(STDIN))

# make backup file for $table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))

#this has to be a global variable in case the last day of the month has negative productiongal
negativePumping = DataFrames.DataFrame(; DataStructures.OrderedDict(Symbol("Date")=>0, Symbol("G-01A")=>0, Symbol("G-02A")=>0, Symbol("G-03A")=>0,  Symbol("G-04A")=>0, Symbol("G-05A")=>0, Symbol("O-01")=>0, Symbol("O-04")=>0, Symbol("PM-01")=>0, Symbol("PM-02")=>0, Symbol("PM-03")=>0, Symbol("PM-04")=>0, Symbol("PM-05")=>0)...)


for m = 1:12
	try
		pumping, runtimes = getdata(m, file)
		negativePumping = insertdata(pumping, runtimes, host, username, password, database, file, table, negativePumping)
	catch
		warn("month $m failed")
	end
end


println("running clean_dbProdDaily.bat...")
run(pipeline(`cat clean_dbProdDaily.bat`,`mysql -u $username -p$password LocalWork`))
