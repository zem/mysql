# We tried to set up this replication for LocalWork, but the step of changing 
# mysql data directory from the default /var/lib/mysql to /lclscratch/mysql/mysql
# only worked on mads01, so we just used mads01 as the master host
# 
# This process could be used, if a master-slave replication is desired.

Set up MySQL master-slave replication for LocalWork (master: madsmin, slave: mads01)
1. find out IP address of the two servers:
$ hostname -I
madsmin 130.55.71.27
mads01 130.55.71.33

CONFIGURE the MASTER
2. update option file on madsmin
/etc/mysql/my.cnf on madsmin has the following lines at the end, which means 
these additional configuration files in these directory are used:
  !includedir /etc/mysql/conf.d/
  !includedir /etc/mysql/mysql.conf.d/
The actuall configuration file is /etc/mysql/mysql.conf.d/mysqld.cnf, in which make the following change:

# binding the server to the local host,Replace the standard IP address with the IP address of server
bind-address            = 130.55.71.27
server-id               = 1
# This is where the real details of the replication are kept. The slave is going to copy all 
# of the changes that are registered in the log. 
# For this step we simply need to uncomment the line that refers to log_bin:   
log_bin                 = /var/log/mysql/mysql-bin.log
# designate the database that will be replicated on the slave server. 
# You can include more than one database by repeating this line for all of the databases you will need. (this line was commented out before)
binlog_do_db            = LocalWork

3. restart the MySQL service: 
sudo service mysql restart

4. In MySQL shell, grant privileges to the slave
CREATE USER 'slave_user'@'%' IDENTIFIED BY 'slave_user';
CREATE USER 'slave_user'@'localhost' IDENTIFIED BY 'slave_user';
GRANT REPLICATION SLAVE ON *.* TO 'slave_user'@'%' IDENTIFIED BY 'slave_user';
GRANT REPLICATION SLAVE ON *.* TO 'slave_user'@'localhost' IDENTIFIED BY 'slave_user';
FLUSH PRIVILEGES;
# can check the users in mysql shell with the following command:
# mysql> select host, user, password from mysql.user;



More Set Up on mads01:
uninstall plugin validate_password;
CREATE USER 'perl'@'localhost' IDENTIFIED BY 'script';
CREATE USER 'perl'@'%' IDENTIFIED BY 'script';
GRANT SELECT ON *.* TO 'perl'@'localhost';
GRANT SELECT ON *.* TO 'perl'@'%';

CREATE USER 'ntao'@'localhost' IDENTIFIED BY 'ntao';
CREATE USER 'ntao'@'%' IDENTIFIED BY 'ntao';
GRANT ALL ON *.* TO 'ntao'@'localhost';
GRANT ALL ON *.* TO 'ntao'@'%';

CREATE USER 'monty'@'localhost' IDENTIFIED BY 'monty';
CREATE USER 'monty'@'%' IDENTIFIED BY 'monty';
GRANT ALL ON *.* TO 'monty'@'localhost';
GRANT ALL ON *.* TO 'monty'@'%';

CREATE USER 'omalled'@'localhost' IDENTIFIED BY 'omalled';
CREATE USER 'omalled'@'%' IDENTIFIED BY 'omalled';
GRANT ALL ON *.* TO 'omalled'@'localhost';
GRANT ALL ON *.* TO 'omalled'@'%';
