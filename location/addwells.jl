import Formatting
import MySQL

data = readcsv("Crwells.csv")
names = data[:, 1]
x_coord = data[:, 2] * 3.28084#meters to feet
y_coord = data[:, 3] * 3.28084#meters to feet
elevation = data[:, 4]
bottom = data[:, 5]
top = data[:, 5]

# ask user for the database username, password and hostname
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))
print("Enter host name: ")
host = chomp(readline(STDIN))

#conn = MySQL.mysql_connect("madsmax", "omalled", "omalled", "LocalWork")
conn = MySQL.mysql_connect(host, username, password, database)
fspec = Formatting.FormatSpec("<8.4f")
for i = 1:length(names)
	deletestring = "delete from WQDBLocation where location_name=\"$(names[i])\";"
	MySQL.mysql_execute(conn, deletestring)
	insertstring = "insert into WQDBLocation (location_name,x_coord,y_coord,coord_uom,surface_elevation) values (\"$(names[i])\",$(Formatting.fmt(fspec, x_coord[i])),$(Formatting.fmt(fspec, y_coord[i])),\"ft\",$(Formatting.fmt(fspec, elevation[i])));"
	MySQL.mysql_execute(conn, insertstring)
	deletestring = "delete from ScreenData where well_name=\"$(names[i])\";"
	MySQL.mysql_execute(conn, deletestring)
	insertstring = "insert into ScreenData (well_name,GEOLOGIC_UNIT_CODE,screen_common_name,open_top_depth,open_bottom_depth) values (\"$(names[i])\",\"UNK\",\"$(names[i])\",$(Formatting.fmt(fspec, top[i])),$(Formatting.fmt(fspec, bottom[i])));"
	MySQL.mysql_execute(conn, insertstring)
end
MySQL.mysql_disconnect(conn)
