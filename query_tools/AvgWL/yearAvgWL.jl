# YearAvgWL.jl
# Query and calculate the average water level(piezometricWLft in GroundWaterLevelData table)
# of all the wells whose screen type(HYDRO_ZONE_TYPE_CODE) are "RT", "R", or "I" for every year

import MySQL
import DataFrames

table = "GroundWaterLevelData"
#calm = 2

database = "LocalWork"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))
print("Enter host name: ")
host = chomp(readline(STDIN))

conn = MySQL.mysql_connect(host, username, password, database)

wellnames = MySQL.mysql_execute(conn, "select distinct(wellname) from $table where wellname != \"\" order by wellname;")
#wellnames = MySQL.mysql_execute(conn, "select distinct(wellname) from GroundWaterLevelData where wellname like 'R-0%' order by wellname;")

GWLdata = DataFrames.DataFrame(wellname=String[],screenname=String[],avg=Float64[],year=Int[],screentype=String[],x_coord=Float64[],y_coord=Float64[])
na_flag = false
for w in wellnames[:wellname]
    well_lrdate = MySQL.mysql_execute(conn, "select max(time) from $table where wellname=\"$w\";")
    well_frdate = MySQL.mysql_execute(conn, "select min(time) from $table where wellname=\"$w\";")
    lry = Dates.year(well_lrdate[1][1])
    fry = Dates.year(well_frdate[1][1])
     
    for y in fry:lry
        command = string("select g.wellname \"well\", p.SCREEN_COMMON_NAME \"screen_name\", avg(g.piezometricWLft) \"avarage\",  YEAR(g.time) \"year\", p.HYDRO_ZONE_TYPE_CODE \"Screen_type\" , w.X_COORD \"X_COORD\", w.Y_COORD \"Y_COORD\" from GroundWaterLevelData g join PortScrn p on (g.wellname=p.LOCATION_NAME and g.portdesc=p.PORT_COMMON_NAME) join WQDBLocation w ON (g.wellname = w.LOCATION_NAME) where g.wellname=\"$w\" and YEAR(g.time)=\"$y\" and (p.HYDRO_ZONE_TYPE_CODE=\"RT\" or p.HYDRO_ZONE_TYPE_CODE=\"R\" or p.HYDRO_ZONE_TYPE_CODE=\"I\") group by p.SCREEN_ID order by p.SCREEN_COMMON_NAME")
        well_year_avg = MySQL.mysql_execute(conn, command)
        # check for NA values in well_year_avg
        for r in 1:size(well_year_avg,1)
            for c in 1:length(well_year_avg)
                if DataFrames.isna(well_year_avg[r,c])
                    if na_flag == false # only need to show a query result/a row in the dataframe once, even it has more than one NA values.
                        warn("well $w has NA value!")
                        @show well_year_avg
                        na_flag = true
                    end
                    #replace NA value with 0, so well_year_avg can be pushed smoothly into GWLdata
                    well_year_avg[r,c] = 0                    
                end
            end
        end
                
        for s =1:size(well_year_avg,1)
            if well_year_avg[s,:avarage] != 0
                push!(GWLdata,Array(well_year_avg[s,:]))
                #push!(year, "$y")
            end
        end
    end # for y
    na_flag = false # prepare for next well
end

# convert coord value to meters
GWLdata[:X_COORDm] = GWLdata[:x_coord] .* 0.3048 #do the column caculation this way instead of using "map", so julia sets the correct datatype for the new column
GWLdata[:Y_COORDm] = GWLdata[:y_coord] .* 0.3048

delete!(GWLdata, :x_coord)
delete!(GWLdata, :y_coord)

outfile = "YearlyAvgWL.txt"
#outfile = "testAvgWL.txt"
DataFrames.writetable(outfile, GWLdata, separator = '\t')
