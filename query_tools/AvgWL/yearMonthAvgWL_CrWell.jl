# YearMonthAvgWL_CrWell.jl
# Calculate the average water level(piezometricWLft in GroundWaterLevelData table) of all the Cr
# wells of a certain month (avgm) in a certain year (avgy).

import MySQL
import DataFrames

table = "GroundWaterLevelData"
#c_wells = ["CdV-R-15-3 S4","CdV-R-37-2 S2","R-01","R-02","R-03","R-04","R-05","R-06","R-07","R-08","R-10a","R-11","R-13","R-14","R-15","R-16r","R-17","R-18","R-19","R-20","R-21","R-23","R-24","R-25","R-27","R-28","R-29","R-30","R-31","R-32","R-33","R-34","R-35b","R-36","R-37","R-38","R-39","R-40","R-41","R-42","R-43","R-44","R-45","R-46","R-47","R-48","R-49","R-50","R-51","R-52","R-53","R-54","R-55","R-56","R-57","R-58","R-60","R-61","R-62","R-63","R-64","R-66","R-67","R-68","SIMR-2"]

database = "LocalWork"
host = "madsmax"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))

conn = MySQL.mysql_connect(host, username, password, database)

#wellnames = MySQL.mysql_execute(conn, "select distinct(wellname) from $table where wellname != \"\" order by wellname;")
wellnames = MySQL.mysql_execute(conn, "select distinct(wellname) from GroundWaterLevelData where wellname like 'Cr%' order by wellname;")

avgy="2017"
avgm="5"

GWLdata = DataFrames.DataFrame(wellname=String[],screenname=String[],avg=Float64[],screentype=String[],x_coord=Float64[],y_coord=Float64[])
for w in wellnames[:wellname]
    #command = string("select g.wellname \"well\", p.SCREEN_COMMON_NAME \"screen_name\", avg(g.piezometricWLft) \"avarage\", p.HYDRO_ZONE_TYPE_CODE \"Screen_type\", w.X_COORD \"X_COORD\", w.Y_COORD \"Y_COORD\" from GroundWaterLevelData g join PortScrn p on (g.wellname=p.LOCATION_NAME and g.portdesc=p.PORT_COMMON_NAME) join WQDBLocation w ON (g.wellname = w.LOCATION_NAME) where g.wellname=\"$w\" and YEAR(g.time)=\"$avgy\" and MONTH(g.time)=\"$avgm\" and p.HYDRO_ZONE_TYPE_CODE=\"RT\" group by p.SCREEN_ID")
    command = string("select g.wellname \"well\", p.SCREEN_COMMON_NAME \"screen_name\", avg(g.piezometricWLft) \"avarage\", p.HYDRO_ZONE_TYPE_CODE \"Screen_type\", w.X_COORD \"X_COORD\", w.Y_COORD \"Y_COORD\" from GroundWaterLevelData g join PortScrn p on (g.wellname=p.LOCATION_NAME and g.portdesc=p.PORT_COMMON_NAME) join WQDBLocation w ON (g.wellname = w.LOCATION_NAME) where g.wellname=\"$w\" and YEAR(g.time)=\"$avgy\" and MONTH(g.time)=\"$avgm\" group by p.SCREEN_ID")
    well_avg = MySQL.mysql_execute(conn, command)

    for s =1:size(well_avg,1)
        push!(GWLdata,Array(well_avg[s,:]))
    end
end

# convert coord value to meters
GWLdata[:X_COORDm] = GWLdata[:x_coord] .* 0.3048
GWLdata[:Y_COORDm] = GWLdata[:y_coord] .* 0.3048

delete!(GWLdata, :x_coord)
delete!(GWLdata, :y_coord)

outfile = avgy * "_" * avgm * "AvgWL_CrWell.txt"

DataFrames.writetable(outfile, GWLdata, separator = '\t')


# query the average for CrIN wells,
CrIN_data = DataFrames.DataFrame(wellname=String[],avg=Float64[],x_coord=Float64[],y_coord=Float64[])
CrIN_wells = MySQL.mysql_execute(conn, "select distinct(wellname) from GroundWaterLevelData where wellname like 'CrIN%' order by wellname;")
for crinw in CrIN_wells[:wellname]
    command = string("select g.wellname \"well\", avg(g.piezometricWLft) \"avarage\", w.X_COORD \"X_COORD\", w.Y_COORD \"Y_COORD\" from GroundWaterLevelData g join WQDBLocation w ON (g.wellname = w.LOCATION_NAME) where g.wellname=\"$crinw\" and YEAR(g.time)=\"$avgy\" and MONTH(g.time)=\"$avgm\" group by g.wellname")
    well_avg = MySQL.mysql_execute(conn, command)

    for s =1:size(well_avg,1)
        push!(CrIN_data,Array(well_avg[s,:]))
    end
end

# convert coord value to meters
CrIN_data[:X_COORDm] = CrIN_data[:x_coord] .* 0.3048
CrIN_data[:Y_COORDm] = CrIN_data[:y_coord] .* 0.3048

delete!(CrIN_data, :x_coord)
delete!(CrIN_data, :y_coord)

outfile_CrIN = avgy * "_" * avgm * "AvgWL_CrINWell.txt"
DataFrames.writetable(outfile_CrIN, CrIN_data, separator = '\t')

#=
open(outfile, "a") do f   
# check for the missing wells against the c_wells list
for cw in c_wells
    if !in(cw, GWLdata[:wellname])
        write(f, "$cw\n")
    end
end
end # open
=#
