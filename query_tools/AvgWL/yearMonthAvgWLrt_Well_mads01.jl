# YearMonthAvgWLrt.jl
# Calculate the average water level(piezometricWLft in GroundWaterLevelData table) of all the
# wells whose screen type(HYDRO_ZONE_TYPE_CODE) is "RT" of a certain month (avgm) in a certain
# year (avgy).
# Check and list the wells which do not have complete record of the month of interest
# Or check wells in the c_wells list and print out the ones which are not in
# the query result list.

import MySQL
import DataFrames

table = "GroundWaterLevelData"
#c_wells = ["CdV-R-15-3 S4","CdV-R-37-2 S2","R-01","R-02","R-03i","R-04","R-05","R-06","R-07","R-08","R-10a","R-11","R-13","R-14","R-15","R-16r","R-17","R-18","R-19","R-20","R-21","R-23","R-24","R-25","R-27","R-28","R-29","R-30","R-31","R-32","R-33","R-34","R-35b","R-36","R-37","R-38","R-39","R-40","R-41","R-42","R-43","R-44","R-45","R-46","R-47","R-48","R-49","R-50","R-51","R-52","R-53","R-54","R-55","R-56","R-57","R-58","R-60","R-61","R-62","R-63","R-64","R-66","R-67","R-68","SIMR-2"]

#c_wells = ["CrEX-1","CrEX-3","CrIN-1","CrIN-2","CrIN-3","CrIN-4","CrIN-5","CrPZ-1","CrPZ-2","CrPZ-3","CrPZ-4","CrPZ-5"]

database = "LocalWork"
host = "mads01"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))

conn = MySQL.mysql_connect(host, username, password, database)

wellnames = MySQL.mysql_execute(conn, "select distinct(wellname) from $table where wellname != \"\" order by wellname;")[1]

avgy="2018"
#avgm="8"
avgm="02"
mstart=DateTime("2018-02-01")
mend=DateTime("2018-03-01")

no_comp_well = DataFrames.DataFrame(wellname=String[], maxtime=DateTime[]) # the list of wells which do not have complete record of the month of interest
GWLdata = DataFrames.DataFrame(wellname=String[],screenname=String[],avg=Float64[],screentype=String[],x_coord=Float64[],y_coord=Float64[])
for w in wellnames[:wellname]
#for w in c_wells
    command = string("select g.wellname \"well\", any_value(p.SCREEN_COMMON_NAME) \"screen_name\", avg(g.piezometricWLft) \"avarage\", p.HYDRO_ZONE_TYPE_CODE \"Screen_type\", any_value(w.X_COORD) \"X_COORD\", any_value(w.Y_COORD) \"Y_COORD\" from GroundWaterLevelData g join PortScrn p on (g.wellname=p.LOCATION_NAME and g.portdesc=p.PORT_COMMON_NAME) join WQDBLocation w ON (g.wellname = w.LOCATION_NAME) where g.wellname=\"$w\" and YEAR(g.time)=\"$avgy\" and MONTH(g.time)=\"$avgm\" and p.HYDRO_ZONE_TYPE_CODE=\"RT\" group by p.SCREEN_ID")
    well_avg = MySQL.mysql_execute(conn, command)[1]

    for s =1:size(well_avg,1)
        push!(GWLdata,Array(well_avg[s,:]))
    end

    maxt = MySQL.mysql_execute(conn, "select max(time) from $table where wellname=\"$w\"")[1][1][1]
    println("$w $maxt")
    if maxt >= mstart && maxt < mend
        push!(no_comp_well,(w,maxt))
    end
end
#@show no_comp_well

# convert coord value to meters
GWLdata[:X_COORDm] = GWLdata[:x_coord] .* 0.3048
GWLdata[:Y_COORDm] = GWLdata[:y_coord] .* 0.3048

delete!(GWLdata, :x_coord)
delete!(GWLdata, :y_coord)

outfile = avgy * "_" * avgm * "AvgWLrt.txt"

DataFrames.writetable(outfile, GWLdata, separator = '\t')

open(outfile, "a") do f   

#=
# check for the missing wells against the c_wells list
for cw in c_wells
    if !in(cw, GWLdata[:wellname])
        write(f, "$cw\n")
    end
end
=#

# write the list of wells which do not have complete record of the month of interest
write(f, "\nwells do not have complete $avgy-$avgm record\nwellname\tmaxtime\n")
for n in 1:size(no_comp_well)[1]
    write(f, "$(no_comp_well[n,1])\t$(no_comp_well[n,2])\n")
end
end # open
