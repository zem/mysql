import MySQL
import DataFrames
import PyPlot
plt=PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb


"""
clean_wl.jl : query water level data for each specified screen for a certain period of time,
            remove barometric pressure, print the original and clean data (both in meters),
            make plot of both data
            (NOTE: cb.denoise removes the first 9 lines of data)

Usage:
        - `include("clean_wl.jl")` (REPL)
	- `julia clean_wl.jl` (terminal)

Arguments:

Defaults:
        - `table` : "GroundWaterLevelData"

Dumps:
        - csv file of original data (default file name: well&screenNameM.csv)
        - csv file of clean data (default file name: well&screenName_cleanM.csv)
        - png file which is the plot of original and clean data (default file name: well&screenNameM.png)
"""


conn = MySQL.mysql_connect("madsmax", "perl", "script", "LocalWork")

# For each screen in 'screens', should be a corresponding well name in 'wells'
wells = ["CrEX-3","CrPZ-2","CrPZ-2","R-11","R-28","R-35a","R-35b","R-36","R-42","R-44","R-44","R-45","R-45","R-50","R-50"]
screens = ["CrEX-3 Screen #1","CrPZ-2 Screen #1","CrPZ-2 Screen #2","R-11 Screen #1","R-28 Screen #1","R-35a Screen #1","R-35b Screen #1","R-36 Screen #1","R-42 Screen #1","R-44 Screen #1","R-44 Screen #2","R-45 Screen #1","R-45 Screen #2","R-50 Screen #1","R-50 Screen #2"]

#wells = ["R-35a"]
#screens = ["R-35a Screen #1"]

table = "GroundWaterLevelData"

observationbegintime = "2017-07-01"
endtime = "2017-08-01"
dateform = Dates.DateFormat("y-m-d H:M:S")

db.connecttodb()

for w in 1:length(wells)
    #latest_record_date = MySQL.mysql_execute(conn, "select max(time) from $table where wellname=\"$(wells[w])\";")
    @show wells[w]
    @show screens[w]
    #@show latest_record_date

    # get water level
    command = string("select g.time \"time\", g.piezometricWLft \"water_level\" from $table g join PortScrn p on (g.wellname=p.LOCATION_NAME and g.portdesc=p.PORT_COMMON_NAME) where g.wellname=\"$(wells[w])\" and p.SCREEN_COMMON_NAME=\"$(screens[w])\" and time>=\"$observationbegintime\" and time<\"$endtime\" order by g.time")
    wl = MySQL.mysql_execute(conn, command)
    wl[:water_level]=wl[:water_level] * 0.3048 # the 0.3048 is to convert from feet to meters, since cb.denoise uses water level in meters
    DataFrames.rename!(wl, :water_level, :water_level_meter)
    
    #-------------------------------
    # remove barometric pressure
    #-------------------------------
    waterlevels=wl[:water_level_meter] 
    waterleveltimes=wl[:time]
    #@show waterlevels
    
    barometricbegintime = Dates.format(DateTime(observationbegintime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
    barometricendtime = Dates.format(DateTime(endtime,dateform)+Dates.Day(1),"yyyy-mm-dd")
    
    # get the barometric information
    baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)
    
    # get the earth tide information -- for now, just read it from a file
    earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)

    @show size(waterlevels)
    # denoise (cb.denoise is using and producing water level in meters. cb.denoise removes the first 9 lines of data)
    goodwaterlevels, goodwaterleveltimes = cb.denoise(waterlevels, waterleveltimes, baropressmb, barotimes, earthtideforces, earthtidetimes)
    
    @show size(goodwaterlevels)
    gwl = DataFrames.DataFrame()
    gwl[:time] = map(x->x, goodwaterleveltimes)
    gwl[:clean_water_level] = map(x->x, goodwaterlevels)
    #--------------------------------------------------------

    outname = wells[w] * "_" * "s" * "$(screens[w][end])" # Doing this instead of just use screens[w], so there is no space in file name
    # print original water level data
    DataFrames.writetable(outname * "M.csv", wl, separator = ',')
    # print water level data with barometric pressure removed
    DataFrames.writetable(outname * "_cleanM.csv", gwl, separator = ',')

    fig = plt.figure(figsize=[10,5])
    plt.plot(goodwaterleveltimes,goodwaterlevels,marker = "+", c="r",label="clean_data")
    plt.plot(wl[:time],wl[:water_level_meter],marker = "x",c="b",label="original_data")
    plt.legend()
    plt.savefig(outname * "M.png")
    plt.close()
end

db.disconnectfromdb()
