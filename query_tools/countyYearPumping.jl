# YearPumping.jl : calculate yearly pumped volumes of Los Alamos County water-supply pumping(table ProdDaily)
# for every well and for all the wells

import MySQL
import DataFrames

wells = ["O-04","PM-01","PM-02","PM-03","PM-04","PM-05"]
#wells = ["G-01","G-01A","G-02","G-02A","G-03A","G-04","G-04A","G-05","G-05A","G-06"] # the G wells
table = "ProdDaily"

database = "LocalWork"
host = "madsmax"
username = "perl"
password = "script"
conn = MySQL.mysql_connect(host, username, password, database)

# calculate yearly productiongal for every well
println("wellname\tyear\tyearly_productiongal")
for w in wells
    well_lrdate = MySQL.mysql_execute(conn, "select max(time) from $table where wellname=\"$w\";")
    well_frdate = MySQL.mysql_execute(conn, "select min(time) from $table where wellname=\"$w\";")
    lry = Dates.year(well_lrdate[1][1])
    fry = Dates.year(well_frdate[1][1])
    
    for y in fry:lry
        command = string("select sum(productiongal) from $table where YEAR(time)=\"$y\" and wellname=\"$w\";")
        well_year_sum = MySQL.mysql_execute(conn, command)
        println("$w\t$y\t$(well_year_sum[1][1])")
    end
    println(" ")
end

# generate string for all the wellnames
w_string = "wellname="
for w in 1:length(wells)
    if w == 1
        w_string = w_string * "\"" * wells[w] * "\""
    else
        w_string = w_string * " or wellname=" * "\"" * wells[w] * "\""
    end
end

command = "select min(time) from $table where " * w_string * ";"
wells_frdate = MySQL.mysql_execute(conn, command)
wells_fry = Dates.year(wells_frdate[1][1])

command = "select max(time) from $table where " * w_string * ";"
wells_lrdate = MySQL.mysql_execute(conn, command)
wells_lry = Dates.year(wells_lrdate[1][1])

# calculate yearly productiongal for all the wells
println("year\tyearly_productiongal_all_well")
for y in wells_fry:wells_lry
    command = "select sum(productiongal) from $table where YEAR(time)=\"$y\" and (" * w_string * ");"
    #@show command
    year_sum = MySQL.mysql_execute(conn, command)
    println("$y\t$(year_sum[1][1])")
end
