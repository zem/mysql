# CrConPlot.jl
# Query Chromium concentration, calculate the average value for each year, and plot the
# queried data and the average

using MySQL
using DataFrames
import PyPlot
plt = PyPlot

#-------------------------------------------------------------------------------
function getchemdata(wells,parameters)
    # wells = ["R-11","R-28",...]
    # parameters = ["Chromium","Sulfate"]
    conn = MySQL.mysql_connect("madsmax", "perl", "script", "LocalWork")
    mytable = "AnalyticalData"
    fields = ["LOCATION_ID", 
              "SAMPLE_DATE",
              "SAMPLE_TIME",
              "PARAMETER_NAME",
#              "ANALYSIS_TYPE_CODE",
#              "SAMPLE_PURPOSE",
#              "FIELD_PREPARATION_CODE",
#              "DETECT_FLAG",
              "REPORT_RESULT",
#              "REPORT_UNITS",
#              "FIELD_SAMPLE_ID",
#              "DILUTION_FACTOR",
#              "BEST_VALUE_FLAG",
#              "LAB_ID",
#              "SAMPLE_USAGE_CODE"
              ]

    groundwaterchemdata = Dict()
    for well in wells
        groundwaterchemdata[well] = Dict()
        for parameter in parameters
            query = "select $(join(fields, ", ")) from $(mytable) where LOCATION_ID='$(well)' and PARAMETER_NAME='$(parameter)';"
            groundwaterchemdata[well][parameter] = mysql_execute(conn, query)
            sort!(groundwaterchemdata[well][parameter], cols = [order(:SAMPLE_DATE)])
            
            # create column SAMPLE_DATE_TIME
            sampledt=[]
            for i = 1:size(groundwaterchemdata[well][parameter],1)
                sd=groundwaterchemdata[well][parameter][:SAMPLE_DATE][i]
                st=groundwaterchemdata[well][parameter][:SAMPLE_TIME][i]
                stsplit=split(st,":")
                if length(stsplit)==2 
                    push!(sampledt,DateTime(Dates.year(sd),Dates.month(sd),Dates.day(sd),parse(Int,stsplit[1]),parse(Int,stsplit[2]),0))
                else
                    push!(sampledt,sd)
                    warn("SAMPLE_TIME is NOT valid, wellname:$well, parameter:$parameter, SAMPLE_DATE:$sd")
                end
            end
            groundwaterchemdata[well][parameter][:SAMPLE_DATE_TIME] = map(x->x,sampledt)
        end
    end
        
    return groundwaterchemdata
end
#-------------------------------------------------------------------------------


conn = MySQL.mysql_connect("madsmax", "perl", "script", "LocalWork")

wells = ["MCOI-4","MCOI-5","MCOI-6","SCI-1","SCI-2","TA-53i"]
#wells = ["MCOI-5"]
parameters = ["Chromium"]

cr_con_data = getchemdata(wells,parameters)

for well in wells
    @show well
    delete!(cr_con_data[well]["Chromium"], :SAMPLE_DATE)
    delete!(cr_con_data[well]["Chromium"], :SAMPLE_TIME)
    
    outname = well * "_CrCon.txt"
#    DataFrames.writetable(outname, cr_con_data[well]["Chromium"], separator = '\t')

    # remove data for MCOI-5 when report_result>50 
    if well == "MCOI-5"
        cr_con_data[well]["Chromium"][:RESULT_F]=map(x->parse(Float64,x), cr_con_data[well]["Chromium"][:REPORT_RESULT])
        cr_con_data[well]["Chromium"]=cr_con_data[well]["Chromium"][cr_con_data[well]["Chromium"][:RESULT_F] .<=30,:]
    end

    
    # calculate average value for each year
    miny=Dates.year(cr_con_data[well]["Chromium"][:SAMPLE_DATE_TIME][1])
    last=size(cr_con_data[well]["Chromium"])[1]
    maxy=Dates.year(cr_con_data[well]["Chromium"][:SAMPLE_DATE_TIME][last])
    #years=Int[]
    yearsp=Date[]
    yavgs=Float64[]
    avg_outname = well * "_avgCrCon.txt"
    open(avg_outname, "w") do f
        write(f, "year\taverage_Cr_concentrations\n")
        y=miny
        @show cr_con_data[well]["Chromium"]
        while y <= maxy
            @show y
            fday = Date("$y" * "-01-01")
            lday = Date("$y" * "-12-31")
            tmp_data = cr_con_data[well]["Chromium"][(cr_con_data[well]["Chromium"][:SAMPLE_DATE_TIME] .>= fday),:]
            tmp_data_rr = tmp_data[(tmp_data[:SAMPLE_DATE_TIME] .<= lday),:][:REPORT_RESULT]
            if length(tmp_data_rr) > 0
                #push!(years, y)
                year_be=Date("$y" * "-01-01")
                year_ed=Date("$y" * "-12-31")
                push!(yearsp, year_be)
                push!(yearsp, year_ed)
                write(f, "$y\t")
                yavg = mean(map(x->parse(Float64,x),tmp_data_rr))
                write(f, "$yavg\n")
                push!(yavgs, yavg) # for beginning of the year 
                push!(yavgs, yavg) # for end of the year
            end
            y = y + 1
        end
    end

    #@show years
    @show yavgs
    # plot data
    f, ax = plt.subplots()
    ax[:plot](cr_con_data[well]["Chromium"][:SAMPLE_DATE_TIME], cr_con_data[well]["Chromium"][:REPORT_RESULT], "b-", marker=".", label="Cr Concentration")
    #plt.ylabel("Cr Concentration")
    ax[:plot](yearsp, yavgs, "r-", marker=".", label="mean of the year")

    ax[:legend](loc="upper center", bbox_to_anchor=(0.5,-0.12))
    #handles, labels = ax[:get_legend_handles_labels]()
    #lgd = ax[:legend](handles,labels,loc="upper center", bbox_to_anchor=(0.5,-0.12))
    ax[:set_title](well)
    outname = well * ".png"
    plt.savefig(outname, bbox_inches="tight")
    
end
