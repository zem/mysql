import MySQL
import DataFrames

"""
latest_record.jl : query the date of the latest record for each well (or each well in the chromium
				wells list) from the input table

Usage:
	- `include("latest_record.jl")` (REPL)
	- `julia latest_record.jl db_table_name output_file_name` (terminal)

Arguments:

Defaults:
	- `db_table_name` : "GroundWaterLevelData"
	- `output_file_name` : "WellLatestRecords.txt"

Examples: 
	- `latest_record(host, username, password, database, table)`
	- `latest_record(host, username, password, database, table, wells_key = wells_key, c_wells = c_wells)`
	- `latest_record(host, username, password, database, table, c_wells = c_wells)`
	- `latest_record(host, username, password, database, table, wells_key = wells_key)`
	- `latest_record(wells_key = wells_key, c_wells = c_wells)`
	- `latest_record(table, wells_key = wells_key, c_wells = c_wells)`
	- `latest_record()`

Dumps:
 - text output file (default file name: WellLatestRecords.txt)

Returns: 
  - wellnames: dataframe of "wellname" and "LatestRecord"
"""

function latest_record(host::String="mads01", username::String="perl", password::String="script", database::String="LocalWork", table::String="GroundWaterLevelData"; wells_key::Array{String,1}=Array(String,0), c_wells::Array{String,1}=Array(String,0))
	# this is the list of tables which can be queried in this function.
	# since the field names are different in different tables, the table name has to be checked here
	tables = ["GroundWaterLevelData", "ProdDaily", "PumpTest"]
	
	if !in(table, tables)
		error("Table \"$table\" is not recognized")
	end
	
	conn = MySQL.mysql_connect(host, username, password, database)
	wellnames = DataFrames.DataFrame(wellname = String[])
	if length(wells_key) != 0 || length(c_wells) != 0
		if length(wells_key) != 0
			wells_mysql = DataFrames.DataFrame()
			for key in wells_key
				wells_mysql = MySQL.mysql_execute(conn, "select wellname from $table where wellname like '%$key%' group by wellname;")[1]
				append!(wellnames, wells_mysql)
			end
		end

		if length(c_wells) != 0
			for c_well in c_wells
				push!(wellnames, [c_well])
			end           
		end
	else
		wellnames = MySQL.mysql_execute(conn, "select distinct(wellname) from $table where wellname != \"\";")[1]   
	end
	sort!(wellnames, cols = DataFrames.order(:wellname))
	display(wellnames)

	lrs = [] 
	if table == "GroundWaterLevelData" || table == "ProdDaily"
		for w = 1:size(wellnames,1)
			wellname = String(wellnames[1][w])
			latest_record_date = MySQL.mysql_execute(conn, "select max(time) from $table where wellname = \"$wellname\";")[1]
			push!(lrs, latest_record_date[1][1])         
		end
	elseif (table == "PumpTest")   
		for w = 1:size(wellnames,1)
			wellname = String(wellnames[1][w])
			latest_record_date = MySQL.mysql_execute(conn, "select max(endtime) from $table where wellname = \"$wellname\";")[1]
			push!(lrs, latest_record_date[1][1])
		end
	end

	wellnames[:LatestRecord] = map(x->x, lrs)
	display(wellnames)
	return wellnames
end # end of function latest_record


database = "LocalWork"
#host = "madsmax"

wells_key = ["CrPZ", "CrIN", "CrEX"] #to search for the well names which includ these strings

#chromium well names
c_wells = ["R-01","R-06","R-07","R-08","R-11","R-13","R-15","R-28","R-34","R-35a","R-35b","R-36","R-42","R-43","R-44","R-45","R-50","R-61","R-62","SIMR-2"]

# giving default values to queried table and out put file
table = "GroundWaterLevelData"
outfile = "WellLatestRecords.txt"

well_lrs = DataFrames.DataFrame[]
if !isinteractive()
	if length(ARGS) == 0
		info("Usage:\nlatest_record.jl db_table_name output_file_name")
		println("default values are used:\ntable name: $table\noutput file name: $outfile")
	elseif length(ARGS) == 1
		table = ARGS[1]
		info("Usage:\nlatest_record.jl db_table_name output_file_name")
		println("default value is used:\noutput file name: $outfile")        
	elseif length(ARGS) == 2
		table = ARGS[1]
		outfile = ARGS[2]
	else
		warn("Too many argument!\nUsage:\nlatest_record.jl db_table_name output_file_name")
		exit(0) #this part is not for julia REPL, so exit works just as desired here
	end
		
	# ask user for the database username and password
	print("Enter MySQL username: ")
	username = chomp(readline(STDIN))
	print("Enter MySQL password: ")
	password = chomp(readline(STDIN))    
        print("Enter host name: ")
        host = chomp(readline(STDIN))
    
	print("Do you want the latest record date of ALL the wells (y/n)? ")
	allwells = chomp(readline(STDIN))
	
	try
		if allwells == "y"
			well_lrs = latest_record(host, username, password, database, table)
		else
			info("Only query the chromium wells!")
			well_lrs = latest_record(host, username, password, database, table, wells_key = wells_key, c_wells = c_wells)
		end
	catch e
		warn("there was an error: $e")
	end
else
	try
		info("Only the chromium wells are queried!")
		well_lrs = latest_record(wells_key = wells_key, c_wells = c_wells)
	catch e
		warn("there was an error: $e")
	end    
end

DataFrames.writetable(outfile, well_lrs)
