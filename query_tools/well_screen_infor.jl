import MySQL
import DataFrames

screen_ids = [3981,231,5008,5009,5010,6121,6112,6103,6184,6105,6106,5006,5007,6202,6203]
outfile = "wellScreenInfor.txt"

database = "LocalWork"
host = "madsmax"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))

conn = MySQL.mysql_connect(host, username, password, database)

screens_infor = DataFrames.DataFrame(SCREEN_ID=Int[],X_COORD=Float64[],Y_COORD=Float64[],SURFACE_ELEVATION=Float64[],OPEN_TOP_DEPTH=Float64[],OPEN_BOTTOM_DEPTH=Float64[],GEOLOGIC_UNIT_CODE=String[],HYDRO_ZONE_TYPE_CODE=String[],SCREEN_COMMON_NAME=String[],DATA_SOURCE=String[],COMMENTS=String[])
                                     
for sid in screen_ids
    command = string("select s.SCREEN_ID, w.X_COORD, w.Y_COORD, w.SURFACE_ELEVATION, s.OPEN_TOP_DEPTH, s.OPEN_BOTTOM_DEPTH, s.GEOLOGIC_UNIT_CODE, s.HYDRO_ZONE_TYPE_CODE, s.SCREEN_COMMON_NAME, s.DATA_SOURCE, s.COMMENTS from WQDBLocation w join ScreenData s on (w.LOCATION_NAME=s.WELL_NAME) where s.SCREEN_ID=$sid;")
    sinfor = MySQL.mysql_execute(conn, command)[1]
    @show sinfor

    if size(sinfor)[1] != 0
        #check for NA
        for c in 1:length(sinfor)
            if DataFrames.isna(sinfor[1,c])
                if c==7 || c==8 || c==9 || c==10 || c==11
                    sinfor[1,c] = ""
                else
                    sinfor[1,c] = 0
                end
            end
        end
    
        push!(screens_infor,Array(sinfor[1,:]))
    end
end

command = string("select s.SCREEN_ID, w.X_COORD, w.Y_COORD, w.SURFACE_ELEVATION, s.OPEN_TOP_DEPTH, s.OPEN_BOTTOM_DEPTH, s.GEOLOGIC_UNIT_CODE, s.HYDRO_ZONE_TYPE_CODE, s.SCREEN_COMMON_NAME, s.DATA_SOURCE, s.COMMENTS from WQDBLocation w join ScreenData s on (w.LOCATION_NAME=s.WELL_NAME) where s.WELL_NAME like 'CrIN%' order by s.WELL_NAME;")

sinforEx = MySQL.mysql_execute(conn, command)[1]

for s in 1:size(sinforEx)[1]
    for c in 1:length(sinforEx)
        if DataFrames.isna(sinforEx[s,c])
            if c==7 || c==8 || c==9 || c==10 || c==11
                sinforEx[s,c] = ""
            else
                sinforEx[s,c] = 0
            end
        end
    end
    
    push!(screens_infor,Array(sinforEx[s,:]))   
end

@show screens_infor
MySQL.mysql_disconnect(conn)

DataFrames.writetable(outfile, screens_infor, separator = '\t')



