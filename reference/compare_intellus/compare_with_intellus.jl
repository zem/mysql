# Id: compare_with_intellus.jl, Mon 06 Feb 2017 09:50:10 AM MST pandeys #
# Created by Sachin Pandey, LANL
# Description: Compare our geochem database with intellus
#------------------------------------------------------------------------------

using MySQL
using DataFrames
using JLD
using PyPlot
plt = PyPlot

conn = MySQL.mysql_connect("madsmax", "perl", "script", "LocalWork")

fields = "LOCATION_ID,
          SAMPLE_DATE,
          PARAMETER_NAME,
          ANALYSIS_TYPE_CODE,
          SAMPLE_PURPOSE,
          FIELD_PREPARATION_CODE,
          DETECT_FLAG,
          REPORT_RESULT,
          REPORT_UNITS,
          FIELD_SAMPLE_ID,
          DILUTION_FACTOR,
          BEST_VALUE_FLAG,
          LAB_ID,
          SAMPLE_USAGE_CODE"
fields = replace(fields,"\n","")
fields = replace(fields,"          ","")

mytable = "AnalyticalData"
wellnames = ["R-11",
             "R-28",
             "R-42",
             "R-43 S1",
             "R-43 S2",
             "R-44 S1",
             "R-44 S2",
             "R-45 S1",
             "R-45 S2",
             "R-50 S1",
             "R-50 S2",
             "R-61 S1",
             "R-61 S2",         
             ]

parameter = "Chromium"

# datelims = Dict()
# mindate = DateTime("2009-03-05T00:00:00")
# maxdate = DateTime("2016-05-10T00:00:00")
# datelims["R-45#2"] = [mindate, maxdate]
datelims = JLD.load("./intellus_datelims.jld","dictionary")

f, ax = plt.subplots(3,5, figsize = (20,10))
data_mysql = DataFrame()
cmap = get_cmap("jet",length(wellnames)+2)
for well in wellnames
    well_intellus = well
    # correct well name to grab intellus data
    if contains(well,"S1")
        well_intellus = replace(well," S1","#1")
    elseif contains(well,"S2")
        well_intellus = replace(well," S2","#2")
    end
    # data from database
    # query = "select $(fields) from $(mytable) 
    #          where LOCATION_ID = '$(well)' 
    #          and PARAMETER_NAME  = '$(parameter)' 
    #          and SAMPLE_DATE > '$(datelims[well_intellus][1])' 
    #          and SAMPLE_DATE < '$(datelims[well_intellus][2])';"
    query = "select $(fields) from $(mytable) 
             where LOCATION_ID = '$(well)' 
             and PARAMETER_NAME  = '$(parameter)';"    
    query = replace(query,"\n","")
    query = replace(query,"         ","")
    data_mysql = mysql_execute(conn, query)
    sort!(data_mysql, cols = [order(:SAMPLE_DATE)])

    # data from database
    data_intellus = readtable(joinpath("intellus_csv_unfiltered","$(well_intellus)_intellus.csv"))

    index = find(x -> x == well,wellnames) # much easier
    @show index
    # ax[index[1]][:plot](data_mysql[:SAMPLE_DATE],data_mysql[:REPORT_RESULT], c = cmap(index[1]), ls = "-", marker = ".", markersize = 10, label = well)
    # ax[index[1]][:plot](DateTime(data_intellus[:Date_Sampled]),data_intellus[:Report_Result], c = cmap(index[1]), ls = "--", marker = "+", markersize = 10)
    ax[index[1]][:plot](data_mysql[:SAMPLE_DATE],data_mysql[:REPORT_RESULT], "b-", marker = ".", markersize = 10, label = well)
    ax[index[1]][:plot](DateTime(data_intellus[:Sample_Date]),data_intellus[:Report_Result], "r--", marker = "+", markersize = 10)
    ax[index[1]][:set_title](well)
    labels = ax[index[1]][:get_xticklabels]()
    for label in labels
        label[:set_rotation](30) 
    end
end
plt.tight_layout()
savefig("compare_with_intellus.png")
plt.close()
