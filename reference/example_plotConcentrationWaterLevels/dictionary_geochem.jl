# Id: dictionary_geochem.jl, Fri 15 Apr 2016 01:29:19 PM MDT pandeys #
# Created by Sachin Pandey, LANL
# Description: Dictionary based upon R42 analysis, 2/16/15
#------------------------------------------------------------------
import DataStructures
import JLD

dict = DataStructures.OrderedDict{AbstractString,AbstractString}(
  # GEN_CHEM
  "Alkalinity-CO3"=>"CO3--",
  "Alkalinity-CO3+HCO3"=>"CO3_HCO3",
  "Bromide"=>"Br-",
  "Calcium"=>"Ca++",
  "Chloride"=>"Cl-",
  "Cyanide (Total)"=>"CN-",
  "Perchlorate"=>"ClO4-",
  "Fluoride"=>"F-",
  "Hardness"=>"Hardness",
  "Potassium"=>"K+",
  "Magnesium"=>"Mg++",
  "Sodium"=>"Na+",
  "Ammonia as Nitrogen"=>"NH3(aq)",
  "Nitrate-Nitrite as Nitrogen"=>"NO3-", # This is actually NO3_NO2
  "Acidity or Alkalinity of a solution"=>"pH",
  "Total Phosphate as Phosphorus"=>"PO4---",
  "Silicon Dioxide"=>"SiO2(aq)",
  "Sulfate"=>"SO4--",
  "Specific Conductance"=>"COND",
  "Total Dissolved Solids"=>"TDS",
  "Total Kjeldahl Nitrogen"=>"TOTN",
  "Total Organic Carbon"=>"TOC",

  # METALS
  "Silver"=>"Ag",
  "Aluminum"=>"Al",
  "Arsenic"=>"As",
  "Boron"=>"B",
  "Barium"=>"Ba",
  "Beryllium"=>"Be",
  "Cadmium"=>"Cd",
  "Cobalt"=>"Co",
  "Chromium"=>"Cr",
  "Copper"=>"Cu",
  "Iron"=>"Fe",
  "Mercury"=>"Hg",
  "Manganese"=>"Mn",
  "Molybdenum"=>"Mo",
  "Nickel"=>"Ni",
  "Lead"=>"Pb",
  "Antimony"=>"Sb",
  "Selenium"=>"Se",
  "Tin"=>"Sn",
  "Strontium"=>"Sr",
  "Thallium"=>"Tl",
  "Uranium"=>"U",
  "Vanadium"=>"V",
  "Zinc"=>"Zn")

  # Stuff in fingerprint data that didn't show up with my intellus query
#  "Chlorate"=>"ClO3",
#  "Chlorine-36/Chlorine Ratio (e-15)"=>"Cl36",
#  "Tritium"=>"3H",
#  "Deuterium Ratio"=>"δ2H",
#  "Nitrogen-15/Nitrogen-14 Ratio(NO3)"=>"δ15N",
#  "Oxygen-18/Oxygen-16 Ratio from Nitrate"=>"δ18O-NO3",
#  "Oxygen-18/Oxygen-16 Ratio"=>"δ18O",
#  "Sulfur-34/Sulfur-32 Ratio (SO4)"=>"δ34S-SO4",
#  "Oxygen-18/Oxygen-16 Ratio from SO4"=>"δ18O-SO4",
#  "Iodine-129/Iodine Ratio (e-15)"=>"I129",
#  "Fraction Modern Carbon (de-normalized)"=>"f14C",
#  "Dioxane[1,4-]"=>"Dioxane",
#  "Acetaminophen"=>"Acetam",
#  "Caffeine"=>"Caffe",
#  "Sulfamethoxazole"=>"Sulfame")

JLD.save("dictionary_geochem_names.jld","dictionary",dict)


dict = DataStructures.OrderedDict{String,Float64}(
  "Br-"=>79.90,
  "Ca++"=>40.08,
  "Cl-"=>35.45,
  "CN-"=>26.02,
  "ClO4-"=>99.45,
  "F-"=>19.00,
  "K+"=>39.10,
  "Mg++"=>24.31,
  "Na+"=>22.99,
#  "NH3(aq)"=>17.03,
  "NH3(aq)"=>14.01, # Ammonia as Nitrogen
#  "NO3-"=>62.00,
  "NO3-"=>14.01, # Nitrate_Nitrite as Nitrogen
  "PO4---"=>94.97,
  "SiO2(aq)"=>60.08,
  "SO4--"=>96.06,
  "CO3--"=>60.01,
  "HCO3-"=>61.02,
  "Ag"=>107.87,
  "Al"=>26.98,
  "As"=>74.92,
  "B"=>10.81,
  "Ba"=>137.33,
  "Be"=>9.01,
  "Cd"=>112.41,
  "Co"=>58.93,
  "Cr"=>51.9961,
  "Cu"=>63.55,
  "Fe"=>55.85,
  "Hg"=>200.59,
  "Mn"=>54.94,
  "Mo"=>95.94,
  "Ni"=>58.69,
  "Pb"=>207.20,
  "Sb"=>121.76,
  "Se"=>78.96,
  "Sn"=>118.71,
  "Sr"=>87.62,
  "Tl"=>204.38,
  "U"=>238.03,
  "V"=>50.94,
  "Zn"=>65.39)
JLD.save("dictionary_geochem_MW.jld","dictionary",dict)

