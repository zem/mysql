using ExcelReaders
using DataFrames
import JLD
using PyPlot
plt = PyPlot
import GWChemData
gwcd = GWChemData

# ---------------------------------------------------------
# User specified
# ---------------------------------------------------------
ARGS = ["R-28", "16922", "Cr,SO4--,NO3-,ClO4-,Tritium", "0.0,0.0,0.0,0.0,0.0", "0.0"]

if length(ARGS) < 5
    println("Arguments must be of the form wellName chem_ndata aqSpec concThreshold waterLevelThreshold")
    println("Check julia dictionary_geochem.jl for aqSpec codes")
    println("e.g. \"R-50#1\" 2593 \"Cr\" 11.0,0.0 1778.0")
    quit()
end

wellName = ARGS[1] # Well name
chem_ndata = parse(Int, ARGS[2]) # number of entries in chem spreadsheet
aqSpec = split(ARGS[3],",") # Dissolved species we are interested in
concThreshold = [parse(Float64, split(ARGS[4],",")[i]) for i in 1:length(split(ARGS[4],","))] # Threshold below which...
waterLevelThreshold = parse(Float64, ARGS[5]) # Threshold below which...

println("Starting $(wellName)")

function grabChemData(aqSpec,concThreshold,df)

    crData = df[df[Symbol("Parameter Name")].==aqSpec,:]

    # Filter: datapoints that are not detected
    crData = crData[(crData[:Detected].=="Y"),:]
    # Filter: Best Value Flag
    crData = crData[(crData[Symbol("Best Value")].=="Y"),:]
    # Filter datapoints you dont like
    crData = crData[(crData[Symbol("Report Result")].>concThreshold),:]
    # Filter: Sample Purpose
    crData = crData[(crData[Symbol("Sample Purpose")].=="REG"),:]

    if aqSpec != "Tritium"
        # Filter: Sample Useage Code
        deleterows!(crData,find(isna(crData[:,Symbol("Sample Usage Code")])))
        crData = crData[(crData[Symbol("Sample Usage Code")].=="INV"),:]
        # Filter: Lab ID
        crData = crData[(crData[Symbol("Lab ID")].=="GELC"),:]
        # Filter: Analysis Type Code
        crData = crData[(crData[Symbol("Analysis Type Code")].=="INIT"),:]
    end

    # Chromium: only filtered data
    if aqSpec == "Cr"
        # Filter: Filtered
        crData = crData[(crData[Symbol("Field Preparation Code")].=="F"),:]
    end

    # Remove data prior to 2008
    crData = crData[Date(crData[Symbol("Sample Date")]).>Date("2008-01-01"),:]

    # ---------------------------------------------------------
    # Subroutine for taking the mean of duplicates and removing
    # ---------------------------------------------------------

    # First get the repeats, non-unique does not work for DateTimes arrays, must convert to string:
    repeats = DataFrame(A=nonunique(DataFrame(A=Dates.format(crData[Symbol("Sample Date")], "yyyy-mm-dd HH:MM:SS"))))
    # note that nonunique returns the first duplicate as "false"

    # If there are repeats...
    if length(repeats[(repeats[:A]),1])>0
        k = 1 # counter for number of duplicate clusters
        tempResults = DataFrame()

        # Find the repeats and put means in tempResults
        i=1
        while i <=length(repeats[:,1])
        #for i in 1:length(repeats[:,1])
            j = i # this is our search index for finding the extents of the cluster of trues
            bull = 0 # flag for searching until next true
            if repeats[i,1]==true
                while bull == 0 #
                    if j > length(repeats[:,1]) # this is required if the last row is a repeat!
                        meanValue = mean(crData[i-1:j-1,:][Symbol("Report Result")])
                        tempResults = vcat(tempResults,crData[i,:])
                        tempResults[k,Symbol("Report Result")]=meanValue
                        i = j
                        bull = 1 # stop while loop for this cluster
                        k = k+1
                    elseif repeats[j,1]==true
                        j = j+1 # continue search for end of cluster
                    elseif repeats[j,1]==false # we found end of cluster
                        meanValue = mean(crData[i-1:j-1,:][Symbol("Report Result")])
                        tempResults = vcat(tempResults,crData[i,:])
                        tempResults[k,Symbol("Report Result")]=meanValue
                        i = j
                        bull = 1 # stop while loop for this cluster
                        k = k+1
                    end
                end
            else
                i = i+1
            end
        end

        # remove all duplicate date times using date time in tempResults
        for i in 1:length(tempResults[:,1])
            crData = crData[(crData[Symbol("Sample Date")].!=tempResults[i,:][Symbol("Sample Date")]),:]
        end

        # join mean data and original cr data
        crData = vcat(crData,tempResults)
    end

    return crData
end

# ---------------------------------------------------------
# Grab concentration data from xlsx
# ---------------------------------------------------------
# For chem data
data_filename = "./intellus_xls/$(wellName).xlsx"

f = openxl(data_filename)
location = readxl(DataFrame, data_filename, "Sheet0!A1:A$(chem_ndata)") # Location ID
dates = readxl(DataFrame, data_filename,    "Sheet0!B1:B$(chem_ndata)") # Date Sampled
names = readxl(DataFrame, data_filename,    "Sheet0!D1:D$(chem_ndata)") # Parameter Name
analysisTypeCode = readxl(DataFrame, data_filename, "Sheet0!F1:F$(chem_ndata)") # Analysis Type Code
samplePurpose = readxl(DataFrame, data_filename, "Sheet0!G1:G$(chem_ndata)") # Sample Purpose
fieldPrepCode = readxl(DataFrame, data_filename, "Sheet0!H1:H$(chem_ndata)") # Field Prep Code
detected = readxl(DataFrame, data_filename, "Sheet0!I1:I$(chem_ndata)") # Detected
conc = readxl(DataFrame, data_filename,     "Sheet0!J1:J$(chem_ndata)") # Report Result
units = readxl(DataFrame, data_filename,    "Sheet0!M1:M$(chem_ndata)") # Report Units
dilution = readxl(DataFrame, data_filename,    "Sheet0!N1:N$(chem_ndata)") # Dilution Factor
bestValueFlag = readxl(DataFrame, data_filename, "Sheet0!S1:S$(chem_ndata)") # Best Value Flag
labID = readxl(DataFrame, data_filename, "Sheet0!T1:T$(chem_ndata)") # Lab ID
fieldSampID = readxl(DataFrame, data_filename, "Sheet0!U1:U$(chem_ndata)") # Sample Usage Code
sampUseageCode = readxl(DataFrame, data_filename, "Sheet0!V1:V$(chem_ndata)") # Sample Usage Code
#times = readxl(DataFrame, data_filename,    "Sheet0!X1:X$(chem_ndata)")
#method = readxl(DataFrame, data_filename,   "Sheet0!T1:T$(chem_ndata)")

include("./dictionary_geochem.jl")
dict_names=JLD.load("./dictionary_geochem_names.jld","dictionary")
dict_MW=JLD.load("./dictionary_geochem_MW.jld","dictionary")

# set the "cool" (short) variable names
for i in 1:length(names[:,1])
    if haskey(dict_names, names[i,1])
        names[i,1] = dict_names[names[i,1]]
    end
end

# make a unifying dataframe
df = DataFrame()
df = [location dates names analysisTypeCode samplePurpose fieldPrepCode detected conc units dilution bestValueFlag labID fieldSampID sampUseageCode]

# ---------------------------------------------------------
# Grab Water levels from mySQL
# ---------------------------------------------------------
# waterleveltimes, waterlevels = gwcd.rawWaterLevels("./reference/yaml/$(wellName).yaml")
waterleveltimes, waterlevels = gwcd.denoiseWaterLevels("./yaml/$(wellName).yaml")
# waterleveltimes, waterlevels = gwcd.denoiseWaterLevels("/scratch/ymp/sach/Working/pumptests/pumptests2016/cr_2016/$(split(wellName,"#")[1])/$(wellName).yaml")

waterleveltimes_datetime = Array(Any,length(waterleveltimes))

# Remove water levels below a certain threshold (first turn into data frame)
waterLevelData = DataFrame(waterleveltimes = waterleveltimes, waterlevels = waterlevels)
waterLevelData = waterLevelData[(waterLevelData[:waterlevels].>waterLevelThreshold),:]

# ---------------------------------------------------------
# Plot
# ---------------------------------------------------------
fig = figure("pyplot_multiaxis",figsize=(20,10))
ax = gca()
title(wellName)

# WATER LEVELS
xlabel("Date")
font = Dict("color"=>"black")
ylabel("Water Levels, [m]",fontdict=font)
p = plt.scatter(waterLevelData[:waterleveltimes],waterLevelData[:waterlevels],color="grey",marker="o",label="Water Level")
setp(ax[:get_yticklabels](),color="black") # Y Axis font formatting
ax[:spines]["left"][:set_color]("black")
ax[:set_ylim](minimum(waterLevelData[:waterlevels])-0.1,maximum(waterLevelData[:waterlevels])+0.1)

new_position = [0.06;0.06;0.74;0.91] # Position Method 2
ax[:set_position](new_position) # Position Method 2: Change the size and position of the axis

colors = ["blue","green","red","orange","purple"]
if length(aqSpec) == 5
    offset = [0.0 1.05 1.10 1.15 1.2]
else
    offset = [0.0 1.06 1.12 1.18]
end

for i = 1:length(aqSpec)
    font = Dict("color"=>colors[i])
    crData = grabChemData(aqSpec[i],concThreshold[i],df)
    sort!(crData, cols = [order(Symbol("Sample Date"))],rev=true)
    if aqSpec[i]=="Cr"
        ax2 = ax[:twinx]() # Create another axis on top of the current axis
        ax2[:set_position](new_position) # Position Method 2: Change the size and position of the axis
        setp(ax2[:get_yticklabels](),color="black") # Y Axis font formatting
        p = ax2[:plot](Date(crData[Symbol("Sample Date")]), crData[Symbol("Report Result")], color=colors[i],marker="o",linewidth=2, markersize = 7)
        ax2[:set_ylabel]("Cr(VI), [ug/L]",fontdict=font)
        setp(ax2[:get_yticklabels](),color=colors[i]) # Y Axis font formatting
        ax2[:spines]["right"][:set_color](colors[i])
    else
        ax3 = ax[:twinx]() # Create another axis on top of the current axis
        ax3[:set_position](new_position) # Position Method 2: Change the size and position of the axis
        setp(ax3[:get_yticklabels](),color="black") # Y Axis font formatting
        ax3[:spines]["right"][:set_position](("axes",offset[i])) # Offset the y-axis label from the axis itself so it doesn't overlap the second axis
        p = ax3[:plot](Date(crData[Symbol("Sample Date")]), crData[Symbol("Report Result")], color=colors[i],marker="o",linewidth=2, markersize = 7)
        if aqSpec[i]=="Tritium"
            ax3[:set_ylabel]("$(aqSpec[i]), [pCi/L]",fontdict=font)
        elseif aqSpec[i]=="ClO4-"
            ax3[:set_ylabel]("$(aqSpec[i]), [ug/L]",fontdict=font)
        else
            ax3[:set_ylabel]("$(aqSpec[i]), [mg/L]",fontdict=font)
        end
        setp(ax3[:get_yticklabels](),color=colors[i]) # Y Axis font formatting
        ax3[:spines]["right"][:set_color](colors[i])
        # Enable just the right part of the frame
        ax3[:set_frame_on](true) # Make the entire frame visible
        ax3[:patch][:set_visible](false) # Make the patch (background) invisible so it doesn't cover up the other axes' plots
        ax3[:spines]["top"][:set_visible](false) # Hide the top edge of the axis
        ax3[:spines]["bottom"][:set_visible](false) # Hide the bottom edge of the axis
    end
    #ax[:set_xlim](minimum(Date(crData[Symbol("Date Sampled")])),maximum(Date(crData[Symbol("Date Sampled")])))
end

axis("tight")
fig[:canvas][:draw]() # Update the figure
#ax2[:ylabel]("Cr(VI), [ug/L]",fontdict=font)

plt.scatter(0, 0, color="grey",marker="o", label = "Water Level")
for i in 1:length(aqSpec)
    plt.plot(0, 0, color=colors[i],marker="o",linewidth=2, markersize=7, label = aqSpec[i])
end
#legend(loc=3)

savefig("./$(wellName).png")
println("Finished $(wellName)")
plt.close()
