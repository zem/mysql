# Id: plot_waterlevel.jl, Thu 13 Oct 2016 02:27:18 PM MDT pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels in Julia
# - Includes removal of bad dates, barometric/earth tide correction
# - Still relies on yaml input file and matplotlib, no command line args
#------------------------------------------------------------------------------
import YAML
import PyPlot
plt=PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb
import GWChemData
gwcd = GWChemData

# User data
wellname = "R-42"
teststarttime = "2015-04-01" # start time for comparison with python 
testendtime = "2015-05-01" # end time for comparison with python 

# Grab and process water levels from mySQL
waterleveltimes, waterlevels = gwcd.denoiseWaterLevels("../yamlFiles/$(wellname).yaml")

# Get water levels and times for test range
dateform = Dates.DateFormat("y-m-d H:M:S")
teststarttimedt = DateTime(teststarttime,dateform)
testendtimedt = DateTime(testendtime,dateform)
testindex = [i for i in 1:length(waterleveltimes) if waterleveltimes[i] .> teststarttimedt && waterleveltimes[i] .< testendtimedt]

# get python results
results_python = readcsv("denoise_python.csv")
for i in 1:length(results_python[:,1])
	results_python[i,1] = DateTime(results_python[i,1], "yyyy-mm-dd HH:MM:SS")
end

# Comparison plot
fig = plt.figure(figsize=[10,5])
plt.scatter(waterleveltimes[testindex],waterlevels[testindex],marker = "x", c="r",label="julia",s=100)
plt.scatter(results_python[:,1],results_python[:,2],marker = "+",c="b",label="python",s=100)
plt.xlim(teststarttimedt,testendtimedt)
plt.legend()
plt.savefig("compare_JuliaPython.png")
plt.close()

# Error stats
max_error_waterlevels =  maximum(waterlevels[testindex] - results_python[:,2])
max_error_times = maximum(waterleveltimes[testindex] - results_python[:,1])
@show max_error_waterlevels
@show max_error_times
