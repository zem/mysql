import aquiferdb as db
import chipbeta as cb
import matplotlib.pyplot as plt
import datetime
import yaml
import csv

def datetimestring2datetime(datetimestr):
	datestr, timestr = datetimestr.split()
	datesplit = datestr.split("-")
	year, month, day = map(int, datesplit)
	timesplit = timestr.split(":")
	hour, minute, second = map(int, timesplit)
	return datetime.datetime(year, month, day, hour, minute, second)

def datetime2datetimestring(dt):
	return datetime2datestring(dt) + " " + ":".join(map(lambda i: str(i).zfill(2), [dt.hour, dt.minute, dt.second]))

def datestring2datetime(datestr):
	splitstr = datestr.split("-")
	return datetime.datetime(*map(int, splitstr))

def datetime2datestring(dt):
	return "-".join(map(lambda i: str(i).zfill(2), [dt.year, dt.month, dt.day]))

# User data
wellname = "R-42"
teststarttime = "2015-04-01" # start time for comparison with python 
testendtime = "2015-05-01" # end time for comparison with python 

with open("../yamlFiles/" + wellname + ".yaml", "r") as f:
	controldata = yaml.load(f)
	f.close()

observationwell = controldata["Observation well"]
observationbegintime = controldata["Observation begin time"]
endtime = controldata["End time"]

try:
	portdescr = str(controldata["Screen number"])
except KeyError:
	portdescr = "SINGLE COMPLETION"
try:
	jumpfixdates = map(lambda x: datetimestring2datetime(x[0]), controldata["Observation jumps"])
	jumpfixsize = map(lambda x: x[1], controldata["Observation jumps"])
except KeyError:
	jumpfixdates = []
	jumpfixsize = []
try:
	samplethreshold = controldata["Sample threshold"]
except KeyError:
	samplethreshold = 0.08
try:
	samplelength = controldata["Sample length"]
except KeyError:
	samplelength = 5
try:
	allbaddates = map(lambda x: [datetimestring2datetime(x[0]), datetimestring2datetime(x[1])], controldata["Bad dates"])
except KeyError:
	allbaddates = []

db.connecttodb()

# raw water levels
waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)

#remove the bad dates
for baddates in allbaddates:
	goodwaterlevels = []
	goodwaterleveltimes = []
	for i in range(len(waterlevels)):
		if not(baddates[0] < waterleveltimes[i] and waterleveltimes[i] < baddates[1]):
			goodwaterlevels.append(waterlevels[i])
			goodwaterleveltimes.append(waterleveltimes[i])
	waterlevels = goodwaterlevels
	waterleveltimes = goodwaterleveltimes

#fix the jumps in the water levels
j = 0
for jumpfixdate in jumpfixdates:
	i = 0
	while i < len(waterleveltimes) and waterleveltimes[i] < jumpfixdate:
		i += 1
	while i < len(waterleveltimes):
		waterlevels[i] += jumpfixsize[j]
		i += 1
	j += 1

#get the barometric information
barometricbegintime = datetime2datestring(datestring2datetime(observationbegintime) + datetime.timedelta(days=-1))
barometricendtime = datetime2datestring(datestring2datetime(endtime) + datetime.timedelta(days=1))
baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)

#get the earth tide information -- for now, just read it from a file
earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)

waterlevelsnosamp, waterleveltimesnosamp = cb.removeSamplingEvents(waterlevels, waterleveltimes, threshold=samplethreshold, samplinglength=samplelength)

goodwaterlevels, goodwaterleveltimes = cb.denoise(waterlevels, waterleveltimes, baropressmb, barotimes, earthtideforces, earthtidetimes)

nosampwaterlevels, nosampwaterleveltimes = cb.denoise(waterlevelsnosamp, waterleveltimesnosamp, baropressmb, barotimes, earthtideforces, earthtidetimes)

#remove the bad dates from the no samp water levels
waterlevels = nosampwaterlevels
waterleveltimes = nosampwaterleveltimes
for baddates in allbaddates:
	goodwaterlevels = []
	goodwaterleveltimes = []
	for i in range(len(waterlevels)):
		if not(baddates[0] < waterleveltimes[i] and waterleveltimes[i] < baddates[1]):
			goodwaterlevels.append(waterlevels[i])
			goodwaterleveltimes.append(waterleveltimes[i])
	waterlevels = goodwaterlevels
	waterleveltimes = goodwaterleveltimes
nosampwaterlevels = waterlevels
nosampwaterleveltimes = waterleveltimes

db.disconnectfromdb()

# Get data processed for test period
teststarttimedt = datetime.datetime.strptime(teststarttime, '%Y-%m-%d')
testendtimedt =  datetime.datetime.strptime(testendtime, '%Y-%m-%d')
testindex = [i for i in range(len(waterleveltimes)) if waterleveltimes[i] > teststarttimedt and waterleveltimes[i] < testendtimedt]

# write to csv
with open("denoise_python.csv", 'w') as mycsvfile:
	thedatawriter = csv.writer(mycsvfile)
	for i in testindex:
		thedatawriter.writerow([goodwaterleveltimes[i],goodwaterlevels[i]])
