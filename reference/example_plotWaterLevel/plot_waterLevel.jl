# Id: plot_waterlevel.jl, Thu 13 Oct 2016 02:27:18 PM MDT pandeys #
# Created by Sachin Pandey, LANL
# Description: Plot water levels in Julia
# - Includes removal of bad dates, barometric/earth tide correction
# - Does not include fixing "jumps"
# - Still relies on yaml input file and matplotlib, no command line args
#------------------------------------------------------------------------------
import YAML
import PyPlot
plt=PyPlot
import PyCall
@PyCall.pyimport aquiferdb as db
@PyCall.pyimport chipbeta as cb

# User data
wellname = "R-42"

yamldata = YAML.load(open("./yamlFiles/$(wellname).yaml"))
observationwell = yamldata["Observation well"]
observationbegintime = yamldata["Observation begin time"]
endtime = yamldata["End time"]

# to convert yaml dates to datetime
dateform = Dates.DateFormat("y-m-d H:M:S")

portdescr=[]
try
  portdescr = string(yamldata["Screen number"])
catch
  portdescr = "SINGLE COMPLETION"
end

jumpfixdates = []
jumpfixsize = []
try
  jumpfixdates = map((x) -> DateTime(x[1],dateform), yamldata["Observation jumps"])
  jumpfixsize  = map((x) -> x[2], yamldata["Observation jumps"])
catch
end

baddates = []
try
  baddates = yamldata["Bad dates"]
catch
end

samplethreshold=[]
try
	samplethreshold = controldata["Sample threshold"]
catch
	samplethreshold = 0.08
end

samplelength=[]
try
	samplelength = controldata["Sample length"]
catch
	samplelength = 5
end

db.connecttodb()

# raw water levels
waterlevels, waterleveltimes = db.getwaterlevels(observationwell, portdescr, observationbegintime, endtime)

fig = plt.figure("pyplot",figsize=(20,15))
plt.subplot(231)
plt.scatter(waterleveltimes,waterlevels)
plt.title("raw data")

# remove the bad dates
for baddate in baddates
	baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
	goodwaterlevels = []
	goodwaterleveltimes = []
  for j in 1:length(waterlevels)
	if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
	else
	  push!(goodwaterlevels,waterlevels[j])
	  push!(goodwaterleveltimes,waterleveltimes[j])
	end
  end
  waterlevels = goodwaterlevels
	waterleveltimes = goodwaterleveltimes
end

plt.subplot(232)
plt.scatter(waterleveltimes,waterlevels)
plt.title("remove bad dates")

# fix the jumps in the water levels
j = 1
for jumpfixdate in jumpfixdates
  i = 1
  while i < length(waterleveltimes) && waterleveltimes[i] < jumpfixdate
	i += 1
  end
  while i < length(waterleveltimes)
	waterlevels[i] += jumpfixsize[j]
	i += 1
  end
  j += 1
end

plt.subplot(233)
plt.scatter(waterleveltimes,waterlevels)
plt.title("fix jumps")

# get the barometric information
barometricbegintime = Dates.format(DateTime(observationbegintime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
barometricendtime = Dates.format(DateTime(endtime,dateform)+Dates.Day(-1),"yyyy-mm-dd")
baropressmb, barotimes = db.getbarometricpressure(barometricbegintime, barometricendtime)

# get the earth tide information -- for now, just read it from a file
earthtideforces, earthtidetimes = db.getearthtide(barometricbegintime, barometricendtime)

waterlevelsnosamp, waterleveltimesnosamp = cb.removeSamplingEvents(waterlevels, waterleveltimes, threshold=samplethreshold, samplinglength=samplelength)
nosampwaterlevels, nosampwaterleveltimes = cb.denoise(waterlevelsnosamp, waterleveltimesnosamp, baropressmb, barotimes, earthtideforces, earthtidetimes)
waterlevels = nosampwaterlevels
waterleveltimes = nosampwaterleveltimes

plt.subplot(234)
plt.scatter(waterleveltimes,waterlevels)
plt.title("denoise")

# remove the bad dates from the no samp water levels
for baddate in baddates
	baddate_datetime = map((x) -> DateTime(x,dateform), hcat(baddate...))
	goodwaterlevels = []
	goodwaterleveltimes = []
	for j in 1:length(waterlevels)
		if waterleveltimes[j] > baddate_datetime[1] && waterleveltimes[j] < baddate_datetime[2]
		else
			push!(goodwaterlevels,waterlevels[j])
			push!(goodwaterleveltimes,waterleveltimes[j])
		end
	end
	waterlevels = goodwaterlevels
	waterleveltimes = goodwaterleveltimes
end

plt.subplot(235)
plt.scatter(waterleveltimes,waterlevels)
plt.title("denoise, remove bad dates")

db.disconnectfromdb()

plt.savefig("results_julia.png")
