import PyPlot

"""
fill_missing.jl : calculate and fill the missing pressure data (last column in ta54_tab_file.txt)
                for ta54 using data from station ta6. And make plot of the original, reference(ta6)
                and calculated data.

Usage:
        - `julia fill_missing.jl ta54data_file ta6data_file`

Dumps:
        - text ouput data file with missing pressure data filled (default file name: ta54_tab_file_revised.txt)
        - plot of the original, reference(ta6) and calculated data (default file name: pressures.png)
"""

ta54data_file = ARGS[1]
ta6data_file = ARGS[2]

#ta6data_file = "raw_ta6_15_201707010000.txt" #data from station ta6 is used as reference data
#ta54data_file = "ta54_tab_file.txt"
headerNum = 7 # number of lines in data file before the first data line

ta6data = readdlm(ta6data_file; skipstart=headerNum)
ta54data = readdlm(ta54data_file; skipstart=headerNum)

# calculate the missing pressure data (last colmn in file)
# the missing data were "*" in data file
ta54data = map(x->x == "*" ? NaN : x, ta54data)
goodis = map(i->!isnan(ta54data[i, end]), 1:size(ta54data, 1))
xhat = map(Float64, ta54data[goodis, end])
x = map(Float64, ta6data[goodis, end])
A = hcat(ones(length(x)), x)
@show typeof(A), typeof(xhat)
coeffs = A \ xhat
ta54datanew = copy(ta54data)
for i = 1:size(ta54datanew, 1)
	if isnan(ta54datanew[i, end])
		ta54datanew[i, end] = coeffs[1] + ta6data[i, end] * coeffs[2]
	end
end

# plot the data
fig, ax = PyPlot.subplots(figsize=(16,9))
ax[:plot](ta6data[:, end], ".", label="TA-6")
ax[:plot](ta54data[:, end], ".", label="TA-54 (original)")
# plot the calculated data to see how well it matchs the original data(none missing part) 
ax[:plot](ta6data[:, end] * coeffs[2] + coeffs[1], "+", label="TA-54 (validation)", alpha=0.5)
ax[:legend]()
fig[:tight_layout]()
fig[:savefig]("pressures.png")
display(fig); println()
PyPlot.close(fig)


# write the revised data in file
outf = open("ta54_tab_file_revised.txt","w")
# add the header lines from input data file so the revised data file has the same format as the
# original data file and can be used as input file for load_Baro.jl 
open(ta54data_file) do inf
    l = 1
    while l <= headerNum
        line = readline(inf)
        if l == 1
            write(outf, "This is REVISED data. ")
        end
        write(outf, line)
        l += 1
    end
end
writedlm(outf, ta54datanew)
close(outf)

