import MySQL
import DataFrames

"""
load_Baro.jl : load baro(weather machine) data (ta54_tab_file.txt) into LocalWork table TA54BaroData

Usage:
        - `julia load_Baro.jl input_file`

Defaults:
        - `table` : "TA54BaroData"

Dumps:
        - backup file for the table (default file name: LocalWork_TA54BaroDataDateTime,sql)
"""

input_file = ARGS[1]
#input_file = "ta54_tab_file.txt"
database = "LocalWork"
table = "TA54BaroData"
#table = "tmpTA54BaroData"

# ask user for the database username and password
print("Enter MySQL username: ")
username = chomp(readline(STDIN))
print("Enter MySQL password: ")
password = chomp(readline(STDIN))
print("Enter host name: ")
host = chomp(readline(STDIN))

# make backup file for the table
dateTime = string(DateTime(now()))
backupName = (database * "_" * table * dateTime * ".sql")
run(pipeline(`mysqldump -u $username -p$password $database $table`, backupName))


# readtable translate all the characters in 'nastrings' list into the dataframe as 'NA'
baro_data = DataFrames.readtable(input_file, separator = '\t', nastrings=["*"], header = false,  names=[:mon, :day, :year, :hour, :min, :doy, :temp, :press], skipstart = 7)

baro_data[:time] = map((mon,d,y,h,min) -> DateTime(y,mon,d,h,min,0), baro_data[1],baro_data[2],baro_data[3],baro_data[4],baro_data[5])
@show baro_data

# check to see if there is NA value in baro_data 
hasNA = false;
for row in 1:size(baro_data,1)
    if DataFrames.isna(baro_data[row, :temp])
        hasNA = true;
        println("temperature for $(baro_data[:time][row]) is NA")
    end

    if DataFrames.isna(baro_data[row, :press])
        hasNA = true;
        println("pressure for $(baro_data[:time][row]) is NA")
    end
end

insert_data = "y"
if hasNA == true
    println("\nThere is invalid data in the input file!\nfill_missing.jl could be used to calculate the missing data.\nDo you still want to insert the data into $database table $table(y/n)?")
    insert_data = chomp(readline(STDIN))
end

if insert_data == "y"
    if hasNA == true
        println("Invalid data will be inserted as 10000 ...")
        # convert temperature NA values into 10000
        baro_data[:temp] = convert(Array, baro_data[:temp], 10000)
        # convert pressure NA values into 10000
        baro_data[:press] = convert(Array, baro_data[:press], 10000)
    end
    @show baro_data
    
    conn = MySQL.mysql_connect(host, username, password, database)

    for i = 1:size(baro_data,1) #size(data,1) returns the number of rows in $data
        time = baro_data[:time][i]
        temp = baro_data[:temp][i]
        press = baro_data[:press][i]
        
        commandstring = string("insert into $table (time, tempC, pressmb) values (\"$time\", $temp, $press)")
        #@show commandstring
        MySQL.mysql_execute(conn, commandstring)
    end
    MySQL.mysql_disconnect(conn)
else
    println("exiting without insert data...")
end
